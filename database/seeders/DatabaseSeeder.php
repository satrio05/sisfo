<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $role = collect([
        //     [
        //         'id'    => 1,
        //         'name'  => 'superadmin',
        //         'status' => '1'
        //     ],
        //     [
        //         'id'    => 2,
        //         'name'  => 'admin',
        //         'status' => '1'
        //     ],
        //     [
        //         'id'    => 3,
        //         'name'  => 'public',
        //         'status' => '1'
        //     ]
        // ])
        //     ->each(function ($role) {
        //         \App\Models\Master\Role::create($role);
        //         \App\Models\User::factory()->count(10)->create([
        //             'roles_id'   => $role['id']
        //         ]);
        //     });
        \App\Models\Admin\Classes::factory(1000)->create();
        // \App\Models\User::factory(10)->create();
    }
}
