<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateListBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('list_books', function (Blueprint $table) {
            $table->id();
            $table->string('slug', 50);
            $table->string('name_books');
            $table->foreignId('publisher_id')->nullable()->references('id')->on('publishers')->onDelete('cascade');
            $table->foreignId('types_id')->nullable()->references('id')->on('types')->onDelete('cascade');
            $table->bigInteger('totals_page');
            $table->string('writer');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('list_books');
    }
}
