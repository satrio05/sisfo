<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterTableTeacherFiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('teacher_files', function (Blueprint $table) {
            // $table->id();
            $table->foreignId('teachers_id')->after('type')->references('id')->on('teachers')->onDelete('cascade');
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return voids
     */
    public function down()
    {
        Schema::table('teacher_files', function (Blueprint $table) {
            // $table->dropColumn(['teacher']);
            $table->dropForeign('teachers_id');
        });
        // Schema::dropIfExists('teacher_files');
    }
}
