<?php

namespace Database\Factories\Admin;

use Illuminate\Database\Eloquent\Factories\Factory;
use \App\Models\Admin\Classes;

class ClassesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Classes::class;

    public function definition()
    {
        return [
            'name'  => $this->faker->sentence(1),
            'since'  => 2022,
            'status' => '1',
            'slug'  => $this->faker->slug(1)
        ];
    }
}
