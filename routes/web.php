<?php

use Faker\Guesser\Name;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

Route::middleware('isLogin')->group(function () {
    Route::get('/', function () {
        return view('auth.login');
    });
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', function () {
        return view('home');
    })->name('home');
    Route::group(['prefix' => 'admin', 'namespace' => '\App\Http\Controllers\Admin'], function () {
        // Route::get('/classes', 'ClassesController@index')->name('admin/classes');
        Route::group(['prefix' => 'books'], function () {
            Route::get('/books-role', 'Books\RoleController@booksRole')->name('books-role');
            Route::get('/publisher-info/{publisher:id}', 'Books\PublishersController@show')->name('admin/books/books-publisher');
            Route::get('publisher/options', 'Books\PublishersController@options')->name('admin/books/publisher/options');
            Route::post('/books-publishers', 'Books\PublishersController@data')->name('admin/books/books-publishers');
            Route::post('/publishers-add', 'Books\PublishersController@store')->name('/admin/books/publishers-add');
            // Route::get('/types', 'Books\TypesController@index')->name('admin/books/types');

            Route::delete('/publishers-destroy', 'Books\PublishersController@destroy')->name('admin/books/publishers-destroy');

            Route::put('/publisher-update/{publisher:id}', 'Books\PublishersController@update')->name('admin/books/publishers-update/publisher-update');

            Route::group(['prefix' => 'list-books', 'namespace' => 'Books'], function () {
                Route::get('/', 'ListController@index');
                Route::get('/show/{listBooks:slug}', 'ListController@show');

                Route::post('/data', 'ListController@data');

                Route::delete('/destroy', 'ListController@destroy');
                // Route::post('/add', 'ListController@store');
            });
            Route::put('/list-books-update/{listBooks:slug}', 'Books\ListController@update');
            Route::post('/list-books-add', 'Books\ListController@store');

            // Route::resource('/list-books', 'Books\ListController');
            // Route::post('/list-books-data', 'Books\ListController@data')->name('admin/books/list-books-data');
        });
        Route::group(['prefix' => 'types'], function () {
            Route::get('/types-show/{id}', 'TypesController@show')->name('admin/types/types-show');
            Route::get('/options', 'TypesController@options')->name('admin/types/options');

            Route::post('/types', 'TypesController@data')->name('admin/types/types');
            Route::post('/types-add', 'TypesController@store')->name('admin/types/types-add');

            Route::put('/types-update/{id}', 'TypesController@update')->name('admin/types/types-update');

            Route::delete('/types-destroy', 'TypesController@destroy')->name('admin/types/types-destroy');
        });


        Route::group(['prefix' => 'students', 'namespace' => 'Students'], function () {
            Route::get('/role', 'RoleController@index')->name('admin/students/role');
            Route::get('/classes-show/{classes:slug}', 'ClassesController@show')->name('admin/students/classes-show');

            Route::post('/classes-data', 'ClassesController@data')->name('admin/students/classes-data');
            Route::post('/classes-add', 'ClassesController@store')->name('admin/students/classes-add');
            Route::post('/classes-import', 'ClassesController@import')->name('admin/classes-import');
            Route::post('/classes-export', 'ClassesController@export')->name('admin/students/classes-export');

            Route::put('/classes-update/{classes:slug}', 'ClassesController@update')->name('admin/students/classes-update');

            Route::delete('/classes-destroy', 'ClassesController@destroy')->name('admin/students/classes-destroy');
        });
    });

    Route::group(['namespace' => '\App\Http\Controllers\Admin'], function () {
        Route::get('/teachers', 'TeachersController@index');
        Route::get('/teachers/info/{teachers:code_teachers}', 'TeachersController@show');
        Route::post('/teachers-add', 'TeachersController@store');
        Route::post('/teachers-data', 'TeachersController@data');
        Route::put('/teachers/update/{teachers:code_teachers}', 'TeachersController@update')->name('teachers/update');
        Route::delete('/teachers/delete', 'TeachersController@destroy')->name('teachers-destroy');
    });

    Route::group(['prefix' => 'users', 'namespace' => '\App\Http\Controllers\Users'], function () {
        Route::group(['prefix' => 'role'], function () {
            Route::get('/', 'RolesController@index')->name('admin/users/role');
            Route::get('/show/{role:name}', 'RolesController@show')->name('admin/users/role/show');
            Route::post('data', 'RolesController@data')->name('admin/users/role/data');
            Route::post('role-add', 'RolesController@store')->name('admin/users/role/role-add');
            Route::put('/update/{role:name}', 'RolesController@update')->name('users/role/update');
            Route::delete('/destroy', 'RolesController@destroy')->name('admin/users/role/destroy');
        });

        Route::get('/', 'UsersController@index')->name('users');
        Route::get('/options', 'UsersController@options')->name('users/options');
        Route::post('/data', 'UsersController@data')->name('users/data');
    });
});
