<?php

namespace App\View\Components;

use Illuminate\View\Component;

class toolsTable extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */

    // public $tableName;
    public $ColumnsForm;
    public $ColumnImport = [
        'columns' => [['name' => 'file', 'icon' => 'fas fa-upload']]
    ];

    public function __construct($field = null)
    {
        $this->ColumnsForm    = $field ?? null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.tools-table');
    }
}
