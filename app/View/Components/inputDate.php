<?php

namespace App\View\Components;

use Illuminate\View\Component;

class inputDate extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $name;
    // public $placeholder;

    public function __construct($name)
    {
        ddd($name);
        // $this->placeholder = $placeholder;
        $this->name = $name;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-date');
    }
}
