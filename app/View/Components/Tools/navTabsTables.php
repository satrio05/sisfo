<?php

namespace App\View\Components\Tools;

use Illuminate\View\Component;

class navTabsTables extends Component
{
    public $config;
    public $id;
    public function __construct($config, $id)
    {
        $this->config = $config;
        $this->id = $id;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.tools.nav-tabs-tables');
    }
}
