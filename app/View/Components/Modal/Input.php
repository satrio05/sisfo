<?php

namespace App\View\Components\Modal;

use Illuminate\View\Component;

class Input extends Component
{
    public $ColumnsForm;

    public function __construct($config = null)
    {
        $this->ColumnsForm = $config ?? null;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal.input');
    }
}
