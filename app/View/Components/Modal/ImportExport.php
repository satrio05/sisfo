<?php

namespace App\View\Components\Modal;

use Illuminate\View\Component;

class ImportExport extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $config;
    public function __construct($config)
    {
        $this->config = $config;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal.import-export');
    }
}
