<?php

namespace App\View\Components\Modal;

use Illuminate\View\Component;

class TypeModal extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $icon;
    public $animation;
    public $size;
    public $theme;

    public function __construct($config = null)
    {
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal.type-modal');
    }
}
