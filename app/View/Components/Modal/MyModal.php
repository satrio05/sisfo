<?php

namespace App\View\Components\Modal;

use Illuminate\View\Component;

class MyModal extends Component
{
    public $type;
    public $role;

    public function __construct($config)
    {
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.modal.my-modal');
    }
}
