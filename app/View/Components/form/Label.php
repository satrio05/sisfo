<?php

namespace App\View\Components\Form;

use Illuminate\View\Component;

class Label extends Component
{

    public $size;
    public $name;

    public function __construct($config)
    {
        foreach ($config as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.form.label');
    }
}
