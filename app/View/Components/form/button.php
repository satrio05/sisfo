<?php

namespace App\View\Components\form;

use Illuminate\View\Component;

class button extends Component
{
    public $config;
    public $classes;
    public function __construct($classes = null, $config)
    {
        $this->config = $config;
        $this->classes = $classes;
    }

    public function render()
    {
        return view('components.form.button');
    }
}
