<?php

namespace Helpers;

class JsonData
{
  public static function data($obj, $params, $type, $selected = null, $output = null)
  {
    $obj  = $obj->get();
    $data = [];
    foreach ($obj as $key => $val) {
      $json = json_decode($val[$params]);
      $json = (array)$json;
      if ($type == 3)
        $json[$selected] = $val[$selected];
      $json['id'] = $val['id'];
      if ($output == 'one') $data = $json;
      else
        $data[]     = $json;
    }
    return $data;
  }

  public static function validate($data, $value, $filter = null)
  {
    $msg = [];
    foreach ($data as $key => $val) {
      foreach ($filter as $keys) {
        if ($value[$keys] != $val[$keys]) {
          unset($filter[0]);
          $msg[$keys] =  $value[$keys];
        }
      }
    }
    if ($msg < $filter)
      return __('Data duplicated.!');
    return $msg;
  }
}
