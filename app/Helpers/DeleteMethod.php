<?php

namespace App\Helpers;

trait DeleteMethod
{

  public static function DeletedData($table, $value, $id): bool
  {
    if ($value == 'soft') {
      $table::whereIn('id', $id)->delete();
      return true;
    } elseif ($value == 'force') {
      $table::withTrashed()->whereIn('id', $id)->forceDelete();
      return true;
    } else if ($value == 'restore') {
      $table::withTrashed()->whereIn('id', $id)->restore();
      return true;
    }
    return false;
    // if (!$table) throw new ErrorException(__('Failed process data'));
  }
}
