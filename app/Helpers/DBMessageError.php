<?php

namespace Helpers;


class DBMessageError
{

  public static function getMessageError($code)
  {
    $errorCodeMessage = [
      101 => 'Query tidak diizinkan.!',
      102 => 'Sintaks tidak valid.!',
      23000 => 'Data sudah ada.!',
    ];

    if (array_key_exists($code, $errorCodeMessage)) {
      $message = $errorCodeMessage[$code];
    }

    return $message;
  }
}
