<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    use HasFactory;

    protected $fillable = [];
    protected $hidden = ['created_at', 'updated_at'];

    public function Publisher()
    {
        return $this->hasOne(Publisher::class);
    }
}
