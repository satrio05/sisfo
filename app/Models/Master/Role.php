<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;

    protected $guarded = [];

    protected $hidden = ['created_at', 'deleted_at'];

    public function users()
    {
        return $this->hasMany(\App\Models\User::class, 'roles_id', 'id');
    }
}
