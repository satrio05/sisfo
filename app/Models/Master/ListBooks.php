<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ListBooks extends Model
{
    use SoftDeletes;
    use HasFactory;

    protected $table = 'list_books';

    protected $fillable = ['publisher_id', 'types_id', 'name_books', 'totals_page', 'writer'];

    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];

    public function publisher()
    {
        return $this->hasOne(Publisher::class, 'id', 'publisher_id');
    }

    public function type()
    {
        return $this->hasOne(Type::class,  'id', 'types_id');
    }
}
