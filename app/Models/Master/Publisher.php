<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publisher extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'company_name', 'location', 'phone_number'];

    protected $hidden = ['created_at', 'updated_at', 'phone_number'];

    public static function PhoneNumberFormated($value)
    {
        return substr_replace($value, "***-***", 4);
    }
}
