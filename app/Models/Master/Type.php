<?php

namespace App\Models\Master;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Type extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $guarded = [];
    protected $hidden = ['created_at', 'deleted_at', 'updated_at'];

    public function scopeFilters($query, $type, $params = null)
    {
        return $query->where(['id' => $type])->orwhere(function ($query) use ($type, $params) {
            $query->where(['id' => $type, 'name' => $params])->orwhere(function ($query) use ($type, $params) {
                $query->where('group_id', $type)->orWhere(function ($query) use ($type, $params) {
                    $query->where(['group_id' => $type, 'name' => $params])->orwhere(function ($query) use ($type) {
                        $query->where(['name' => $type]);
                    });
                });
            });
        });
    }
}
