<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'roles_id'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'roles_id',
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function setPasswordAttribute($password)
    {
        $password = empty($password) ? 1234 : $password;
        $this->attributes['password'] = bcrypt($password);
    }

    public function roles()
    {
        return $this->belongsTo(\App\Models\Master\Role::class);
    }

    public function scopeOptions($query, $filter)
    {
        return $query->where('email', 'like', '%' . $filter . '%')->whereNull('roles_id')->orwhere(function ($query) {
            $query->whereNotIn('roles_id', [1]);
        });
    }
}
