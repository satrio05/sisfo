<?php

namespace App\Models\Admin;

use Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $guarded = [];

    protected $hidden = ['created_at', 'updated_at', 'id', 'delted_at'];

    protected function getNameAttribute()
    {
        return \Illuminate\Support\Str::title($this->attributes['name']);
    }

    protected function getEducationsAttribute()
    {
        $edu = json_decode($this->attributes['educations'], true);
        return \Illuminate\Support\Str::title($edu['last'] . ' - ' . $edu['school'] . ' - ' . $edu['accreditation']);
    }

    // protected function getAddressAttribute()
    // {
    //     return \Illuminate\Support\Str::limit($this->attributes['address'], 30, '...');
    // }

    protected function profile($id = null)
    {
        // $teacherFile = $this->teachersFile();
        // $teacherFile->map(function ($value) {
        $teacherFile = $this->teachersFile()->select('id', 'type', 'file');
        if (!is_null($id))
            return $teacherFile->where(['teachers_id' => $id]);
        else
            return $teacherFile->where(['type' => 'foto']);
        // $teacherFileData = $teacherFile->map(function ($value) {
        // return ['id' => $value['id'], 'type' => $value['id'], 'file' => $value['file']];
        // });
        // ->map(function ($value) {
        //     return $value;
        // });

        // });
    }

    public static function urlUploadFile($name = null, $file = null)
    {
        // return  url('storage/' . $name . '/' . hash($file));
        // return 
    }

    public function teachersFile()
    {
        return $this->hasMany(TeacherFile::class, 'teachers_id', 'id');
    }
}
