<?php

namespace App\Models\Admin;

use App\Http\Controllers\Admin\Teachers;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherFile extends Model
{
    use HasFactory;

    protected $table = 'teacher_files';

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];

    protected $fillable = ['teachers_id', 'type', 'file'];

    public function teacher()
    {
        return $this->belongsTo(Teacher::class);
    }
}
