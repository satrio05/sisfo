<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'slug', 'since', 'status'];

    public $timestamps = true;

    protected $hidden = ['created_at', 'updated_at'];

    public function setSlugAttribue($slug = null)
    {
        // $slug = empty($slug) ? $this->name : $slug;
        $this->attributes['slug'] = \Illuminate\Support\Str::kebab($slug);
    }

    public function setNameAttribute($name = null)
    {
        $this->attributes['name']  = \Illuminate\Support\Str::lower($name);
    }

    public function getNameAttribute($value)
    {
        return \Illuminate\Support\Str::ucfirst($value);
    }
}
