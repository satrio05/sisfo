<?php

namespace App\Imports\Admin;

use Illuminate\Support\Collection;
use \App\Models\Admin\Classes;

use Maatwebsite\Excel\Concerns\{
    ToCollection,
    Importable,
    WithHeadingRow,
    WithValidation,
    WithStartRow,
    WithChunkReading
};

class ClassesImport implements
    ToCollection,
    WithHeadingRow,
    WithValidation,
    WithStartRow,
    WithChunkReading
{

    use Importable;

    public function startRow(): int
    {
        return 3;
    }

    public function rules(): array
    {
        return [
            '*.name'    => 'required|max:50|unique:classes,name|regex:/^[a-zA-Z \s]+$/',
            '*.slug'    => 'required|max:50|unique:classes,slug|regex:/^[a-zA-Z \s\-]+$/',
            '*.status'  => 'nullable|in:0,1',
            '*.year'    => 'required|numeric|digits:4'
        ];
    }

    public function collection(Collection $collection)
    {
        foreach ($collection->unique('slug')->toArray() as $key => $value) {
            Classes::insertOrIgnore([
                'name'      => $value['name'],
                'slug'      => is_null($value['slug']) ? \Illuminate\Support\Str::slug($value['name']) : \Illuminate\Support\Str::slug($value['slug']),
                'since'     => $value['year'],
                'status'    => !is_null($value['status']) ? '' . $value['status'] . '' : '0',
                'created_at' => \Carbon\Carbon::createFromDate(now())
            ]);
        }
    }

    public function chunkSize(): int
    {
        return 10000;
    }
}
