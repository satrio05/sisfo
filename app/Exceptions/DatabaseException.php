<?php

namespace App\Exceptions;

use Exception;
use Helpers\DBMessageError;

class DatabaseException extends Exception
{

    function render($request)
    {
        $message = DBMessageError::getMessageError($this->getMessage());
        return response()->json(['errors' => $message], 302);
    }
}
