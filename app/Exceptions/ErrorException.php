<?php

namespace App\Exceptions;

use Exception;

class ErrorException extends Exception
{
    public function render($request)
    {
        return response()->json([
            // 'message' => 'invalid proccessing data.!',
            'errors' => [$this->getMessage()]
        ], 302);
    }
}
