<?php

namespace App\Http\Requests\Users;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->request->has('users'))
            return [
                'users.*' => "required",
                'name'  => "required|unique:roles,name,{$this->role->id},id"
            ];

        return [
            'name'  => "required|unique:roles,name,{$this->role->id},id"
        ];
    }
}
