<?php

namespace App\Http\Requests\Admin\Teachers;

use Illuminate\Foundation\Http\FormRequest;

class AddBioTeacher extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth()->check();
    }

    public function rules()
    {
        if ($this->hasFile('file'))
            return ['file'  => 'required|mimes:jpg,png,jpeg|max:1000'];
        else
            return  [
                'nik'           => 'required|numeric|digits:16|unique:teachers,nik,null,id',
                'name'          => 'required|regex:/^[a-zA-Z \.\s]+$/|max:100',
                'educations'    => 'required||not_in:null,0',
                'name_school'   => 'required',
                'accreditation' => 'required|size:1',
                'statuses'      => 'required|not_in:null,0',
                'address'       => 'required',
                'file'          => 'nullable|mimes:jpg,png,jpeg|max:1000'
            ];
    }
}
