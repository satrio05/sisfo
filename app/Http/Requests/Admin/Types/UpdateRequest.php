<?php

namespace App\Http\Requests\Admin\Types;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->type == 3)
            return [
                'type'   => 'required|numeric',
                'grade'  => "required|regex:/^[a-zA-Z \s]+$/|max:50",
                'score'  => "required|numeric|digits_between:2,3|max:100"
            ];
        else
            return [
                'name'  => "required",
                'type'  => "required"
            ];
    }
}
