<?php

namespace App\Http\Requests\Admin\Books;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PublisherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->only('types')) {
            return [
                'company_name.*'      => 'required|max:50|regex:/^[a-zA-Z0-9 \s ]+$/',
                'name.*'              => "nullable|unique:publishers,name|regex:/^[a-zA-Z0-9 \s]+$/",
                'phone_number.*'      => 'required|regex:/^[0-9]+$/|numeric|digits_between:10,16',
                'location.*'          => 'required|max:200'
            ];
        } else {
            return [
                'company_name'      => 'required|max:50|regex:/^[a-zA-Z0-9 \s ]+$/',
                'name'              => "nullable|unique:publishers,name|regex:/^[a-zA-Z0-9 \s]+$/",
                'phone_number'      => 'required|regex:/^[0-9]+$/|numeric|digits_between:10,16',
                'location'          => 'required|max:200'
            ];
        }
    }
}
