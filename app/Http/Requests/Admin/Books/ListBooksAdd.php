<?php

namespace App\Http\Requests\Admin\Books;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class ListBooksAdd extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_books'    => "required|regex:/^[a-zA-Z0-9 \s]+$/|unique:list_books,name_books,NULL,id,types_id,{$this->types_id},publisher_id,{$this->publisher_id}",
            'publisher_id'  => 'required|not_in:null,0',
            'types_id'      => 'required|not_in:null,0',
            'writer'        => 'required|max:100',
            'totals_page'   => 'required|numeric'
        ];
    }
}
