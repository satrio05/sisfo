<?php

namespace App\Http\Requests\Admin\Books;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class PublisherUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name'      => 'required|max:50|regex:/^[a-zA-Z0-9 \s]+$/',
            'name'              => "required|unique:publishers,name,{$this->publisher['id']},id,name,{$this->name}|regex:/^[a-zA-Z0-9 \s\-]+$/",
            'phone_number'      => 'required|numeric|digits_between:10,16',
            'location'          => 'required|max:200'
        ];
    }
}
