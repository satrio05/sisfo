<?php

namespace App\Http\Requests\Admin\Classes;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class AddRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->only('types')) {
            return [
                'name.*'      => 'required|max:50|unique:classes,slug|regex:/^[a-zA-Z ]+$/',
                'slug.*'      => "nullable|unique:classes,slug|regex:/^[a-zA-Z \s]+$/",
                'since.*'     => 'required|numeric|digits:4',
                'status.*'    => 'nullable|in:0,1'
            ];
        } else {
            return [
                'name'      => 'required|max:50|regex:/^[a-zA-Z ]+$/',
                'slug'      => "nullable|unique:classes,slug|regex:/^[a-zA-Z \s]+$/",
                'since'     => 'required|numeric|digits:4',
                'status'    => 'nullable|in:0,1'
            ];
        }
    }
}
