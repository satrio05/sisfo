<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\ErrorException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Types\{AddRequest, UpdateRequest};
use App\Models\Master\Type;
use Illuminate\Http\Request;
use DataTables;
use Helpers\JsonData;
use Illuminate\Database\QueryException;

class TypesController extends Controller
{
    // 
    public function index($types = null)
    {
        $model =  Type::select('id', 'name', 'group_id')->filters($types)->latest();
        if ($types != 3)
            $model = $model;
        else
            $model = JsonData::data($model, 'name', $types, 'group_id');

        return $model;
    }

    protected function setTabName($id, $type = null): string
    {
        if ($id == 1)
            $tabName = ['categori-books', 1];
        if ($id == 2)
            $tabName = ['statuses-student', 2];
        if ($id == 3)
            $tabName = ['grade-score-student', 3];
        if (is_null($type)) return $tabName[0];
        return $tabName[1];
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            $filters = $request->group;
            if ($filters) {
                $model      = $this->index($filters, ['id', 'name']);
            }
            return Datatables::of($model)
                ->addIndexColumn()
                ->escapeColumns($filters == 3 ? ['id',  'grade', 'score'] : ['id', 'name'])
                ->addColumn('action', function ($row) use ($userAccess, $filters) {
                    $btn = '<button class="delete btn btn-danger btn-sm"  data-route="../types/types-destroy" data-id="' . $row['id'] . '" ><i class="fa fa-trash mr-2"></i>Delete</button>';
                    if ($filters < 3)
                        $btn = $btn . '<button class="update btn btn-warning btn-sm" data-route="../types/types-show" id="edit-form" data-name="../types/types-update/' .  $row['name'] . '" data-id="' . $row['name'] . '" ><i class="fa fa-edit mr-2"></i>Update</button>';
                    if ($filters == 3)
                        $btn = $btn . '<button class="update btn btn-warning btn-sm" data-route="../types/types-show" id="edit-form" data-name="../types/types-update/' .  $row['id'] . '" data-id="' . $row['id'] . '" ><i class="fa fa-edit mr-2"></i>Update</button>';
                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action'])

                ->toJson();
        }
    }

    public function options(Request $request)
    {
        $filter  = trim($request->filters);
        $term  = trim($request->search);
        if (empty($term)) return response()->json([]);
        if (!empty($term)) {
            $data = [];
            $model = Type::where(['group_id' => (int)$filter])->where('name', 'like', '%' . $term . '%')->get();
            foreach ($model as $values) {
                $data[] = ['id' => $values->id, 'name' => $values->name];
            }
            return response()->json($data);
        }
    }


    public function store(AddRequest $request)
    {
        $validate = $request->validated();
        // $match = true;
        if ($request->type == 3) {
            $obj = $this->index($request->type);
            $validate['group_id'] = $validate['type'];
            $data = JsonData::validate($obj, $validate, ['group_id', 'grade', 'score']);
            if (is_string($data)) throw new \App\Exceptions\ErrorException($data);
            $value = json_encode($data);
        }
        $data = [
            'group_id' => $validate['type'],
            'name'     => $request->type == 3  ? $value :  $validate['name']
        ];
        try {
            $Add      = Type::create($data);
            return response()->json(['message' => __('Insert data successfully.!'), 'table' => 'table-' . $this->setTabName($validate['type'])], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }


    public function show($id)
    {
        $types =  $this->index($id);
        if (is_numeric($id)) {
            $model = JsonData::data($types, 'name', null, 'group_id', 'one');
            $message = [
                'text'  => [
                    'grade'  => $model['grade'],
                ],
                'number'    => [
                    'id'    => $model['id'],
                    'score' => $model['score']
                ]
            ];
        } else {
            $types = $types->first();
            $message = [
                'text'  => [
                    'name'  => $types->name,
                    'id'    => $types->name
                ],
            ];
        }
        return response()->json(['message' => $message], 200);
        // $keys = explode(':', $id);
        // $Delete = \App\Models\Master\Type::where(['type' => $keys[0]]);
        // $valuesJson = $Delete->pluck('data');
        // $valuesJson = json_decode($valuesJson[0], true);
        // $data = [];
        // foreach ($valuesJson as $key => $value) {
        //     if ($value['id'] == $keys[1]) {
        //         $data = ['id' => $valuesJson[$key]['id'], 'name' => $valuesJson[$key]['name']];
        //     }
        // }
        // $message = [
        //     'text'  => [
        //         'name' => $data['name'],
        //         'id'    => $data['id']
        //     ],
        // ];
        // return response()->json(['message' => $message], 200);
    }

    public function edit($id)
    {
        //
    }

    public function update($id, UpdateRequest $request)
    {
        $validate = $request->validated();
        if (is_numeric($id)) {
            $name = array_diff_key($validate, array_flip(['type']));
            $name = json_encode($name);
        } else {
            $name = $validate['name'];
        }
        $type = Type::select('id', 'name', 'group_id')->filters($id, $name)->first();
        if ($name == $type->name) throw new \App\Exceptions\ErrorException("No updated data.!");
        try {
            $update = $type->update(['name' => $name]);
            return response()->json(['message' => __('Updated data successfully.!'), 'table' => 'table-' . $this->setTabName($request->type)], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }

        // echo $checkedData->id;
        // echo $id;
        // if ($id === $checkedData->id) {
        //     echo 'asa';
        // } else {
        // }
        // ddd($checkedData->id);
        // die();

        // try {
        //     $types->update(['name' => $validate['name']]);
        //     return response()->json(['message' => __('Updated data successfully.!'), 'table' => 'table-' . $this->setTabName($validate['type'])], 200);
        // } catch (QueryException $th) {
        //     // throw new \App\Exceptions\DatabaseException($th->getCode());

        //     // return response(['errors'    => __('Updated data failed.!')], 422);
        // }
        // if (empty($request->name))  return response(['errors'    => __('Data required.!')], 422);
        // $keys = explode(':', $id);
        // $Update = \App\Models\Master\Type::where(['type' => $keys[0]]);
        // $valuesJson = $Update->pluck('data');
        // $valuesJson = json_decode($valuesJson[0], true);
        // foreach ($valuesJson as $key => $value) {
        //     if ($value['id'] == $keys[1]) {
        //         $valuesJson[$key]['name'] = $request->name;
        //     }
        // }
        // try {
        //     $Update->update(['data' => json_encode($valuesJson)]);
        //     return response()->json(['message' => __('Updated data successfully.!'), 'table' => 'table-types'], 200);
        // } catch (QueryException $th) {
        //     return response(['errors'    => __('Updated data failed.!')], 422);
        // }
    }

    public function destroy(Request $request)
    {
        if (empty($request->id)) throw new ErrorException("Required data selected.!");
        $Delete = Type::find($request->id);
        if (!$Delete) throw new ErrorException("Data not found.!");
        try {
            $Delete->delete();
            return response()->json(['message' => __('Deleted data successfully.!'), 'table' => 'table-' . $this->setTabName($Delete['group_id'])], 200);
        } catch (QueryException $th) {
            throw new ErrorException($th->getMessage());
        }
    }
}
  // $keys = explode(':', $request->id);
        // $Delete = \App\Models\Master\Type::where(['type' => $keys[0]]);
        // $valuesJson = $Delete->pluck('data');
        // $valuesJson = json_decode($valuesJson[0], true);
        // foreach ($valuesJson as $key => $value) {
        //     if ($value['id'] == $keys[1]) {
        //         $valuesJson[$key]['delete'] = true;
        //     }
        // }
        // try {
        //     $Delete->update(['data' => json_encode($valuesJson)]);
        //     return response()->json(['message' => __('Deleted data successfully.!'), 'table' => 'table-types'], 200);
        // } catch (QueryException $th) {
        //     return response(['errors'    => __('Deleted data failed.!')], 422);
        // }