<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Teachers\AddBioTeacher;
use App\Http\Requests\Admin\Teachers\UpdateBioTeacher;
use App\Models\Admin\Teacher;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use DataTables;
use ErrorException;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use ResizeImage;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ColumnsForm = $this->FormColumns([
            ['nik', 'number', 'fa fa-sort-numeric-down', '', 'Entry your number nik'],
            ['name', 'text', 'fa fa-text-width', '', 'Entry your full name'],
            ['educations', 'select', 'fa fa-check', 'no-label', '', ['smp' => 'SMP', 'sma' => 'SMA', 'd3' => 'Diploma III', 's1' => 'Strata 1'], 'md', ''],
            ['name_school', 'text', 'fa fa-text-width', '', 'Entry your name school'],
            ['accreditation', 'select', 'fa fa-check', 'no-label', '', ['a' => 'A', 'b' => 'B', 'c' => 'C', 'd' => 'D', 'e' => 'E'], 'md', ''],
            ['statuses', 'select', 'fa fa-check', 'no-label', '', [1 => 'PNS', 2 => 'HONOR'], 'md', 'Status Employee'],
            ['address', 'textarea', 'fa fa-map-marker', '', 'Entry your address'],
        ]);
        $ColumnsFormUpload = $this->FormColumns([
            ['type_file', 'select', 'fa fa-check', 'no-label', '', ['foto' => 'Profile', 'ktp' => 'KTP', 'ijazah' => 'Ijazah'], 'md', 'Type File'],
            ['file', 'file', 'one', 'choose this file profile']
        ]);
        return view('admin.teachers.index', compact('ColumnsForm', 'ColumnsFormUpload'));
    }


    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess)
                $model = Teacher::with(['teachersFile'])->select('teachers.*')->latest();
            return Datatables::eloquent($model)
                ->addIndexColumn()
                ->editColumn('profile', function ($row) {
                    $params = '';
                    if (sizeof($row->teachersFile) == 0) {
                        $params .= 'undifined';
                    } else {
                        $params .= ((!empty($row->teachersFile[0]->type) || $row->teachersFile[0]->type == 'foto') ? $row->teachersFile[0]->file : 'undifined');
                    }
                    return $params;
                })
                ->editColumn('statuses', function ($row) {
                    return ($row->statuses == 1 ? 'PNS' : ($row->statuses == 2 ? 'Honor' : 'Magang'));
                })
                ->editColumn('address', function ($row) {
                    return \Illuminate\Support\Str::limit($row->address, 30, '...');
                })
                ->editColumn('name', function ($row) {
                    return $row->name;
                })
                ->escapeColumns(['code_teachers', 'name', 'educations', 'statuses', 'address', 'profile'])
                ->removeColumn(['nik', 'id', 'teachers_file'])
                ->skipTotalRecords()
                ->addColumn('action', function ($row) use ($userAccess) {
                    $btn = '<button class="btn btn-info btn-sm" id="info" data-route="teachers/info"  data-id="' . $row->code_teachers . '"><i class="fa fa-info "></i></button>';
                    $btn = $btn . '<button class="btn btn-warning btn-sm" id="edit" data-route="teachers/info" data-name="teachers/update"  data-id="' . $row->code_teachers . '" ><i class="fa fa-pen"></i></button>';
                    if ($row->teachersFile->count() < 1)
                        $btn = $btn . '<button class="delete btn btn-danger btn-sm" method-delete="soft" id="delete" data-route="teachers/delete" data-id="' . $row->code_teachers . '" ><i class="fa fa-trash"></i></button>';
                    $btn = $btn . '<button class="btn btn-primary btn-sm" id="upload-multi-file" data-route="teachers/update"   data-id="' . $row->code_teachers . '" ><i class="fa fa-pen"></i></button>';

                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action', 'profile', 'statuses'])
                ->toJson();
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    // AddBioTeacher
    public function store(AddBioTeacher $request)
    {
        $validate = $request->validated();
        $number = \Illuminate\Support\Str::substr($validate['nik'], 1, 5);
        $code_teachers = $number . '' . \Carbon\Carbon::parse(\Carbon\Carbon::now())->format('Y');
        $teacher = new Teacher;
        $teacher->code_teachers  = $code_teachers;
        $teacher->nik = $validate['nik'];
        $teacher->name = $validate['name'];
        $teacher->statuses = $validate['statuses'];
        $teacher->educations = json_encode(['last' => $validate['educations'], 'school' => $validate['name_school'], 'accreditation' => $validate['accreditation']]);
        $teacher->address = $validate['address'];
        if ($request->has('file')) {
            $Profile = ResizeImage::make($validate['file']->getRealPath());
            $Profile->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $file =  $validate['file']->hashName();
            Storage::disk('public')->makeDirectory($validate['name']);
        }
        try {
            $teacher->save();
            $message = [
                'message'   => __('Insert data successfully.!'),
                'table'     => 'table-teachers'
            ];
            if ($request->hasFile('file')) {
                \App\Models\Admin\TeacherFile::insert([
                    'teachers_id' => $teacher->id,
                    'file'        => $request->getSchemeAndHttpHost() . '/' . 'storage/' . $validate['name'] . '/' . $file,
                    'type'       => 'foto'
                ]);
                $Profile->save(Storage::path(
                    'public/' . $validate['name'] . '/' . $file
                ));
                return response()->json($message, 200);
            }
            if (!$teacher) throw new \App\Exceptions\ErrorException("Failed insert data.!");
            return response()->json($message, 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teachers)
    {
        $nameSchool = explode('-', $teachers->educations);
        return response()->json([
            'message' => [
                'text'  => [
                    'name'          => $teachers['name'],
                    'name_school'   => $nameSchool[1],
                ],
                'select'    => [
                    'accreditation' => \Illuminate\Support\Str::camel($nameSchool[2]),
                    'educations' => \Illuminate\Support\Str::camel($nameSchool[0]),
                    'statuses'  => $teachers['statuses']
                ],
                'textarea' => [
                    'address'   => $teachers['address']
                ],
                // 'file'      => [
                //     'file'  =>     $teachers->profile
                // ],
                'number'  => [
                    'nik'           => $teachers['nik']
                ]
            ]
        ]);
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, Teacher $teachers)
    {
        ddd($request->all());
        $validate = $request->validated();
        if ($request->only('file')) {
            $Profile = ResizeImage::make($validate['file']->getRealPath());
            $Profile->resize(100, 100, function ($constraint) {
                $constraint->aspectRatio();
            });
            $file =  $validate['file']->hashName();
        } else {
            $teachers->name = $validate['name'];
            $teachers->nik  = $validate['nik'];
            $teachers->educations = json_encode(['last' => $validate['educations'], 'school' => $validate['name_school'], 'accreditation' => $validate['accreditation']]);
            $teachers->statuses = $validate['statuses'];
            $teachers->address = $validate['address'];
        }
        try {
            $message = [
                'message'   => __('Successfully update data.!'),
                'table'     => 'table-teachers'
            ];
            if ($request->hasFile('file')) {
                $pathName = $teachers['name'] ?? $teachers->name;
                $path = Storage::path('public/' . $pathName . '/' . $file);
                if (Storage::exists($teachers->name))
                    $Profile->save($path);
                else {
                    Storage::disk('public')->makeDirectory($pathName);
                    $Profile->save($path);
                }
                \App\Models\Admin\TeacherFile::insert([
                    'teachers_id' => $teachers->id,
                    'file'        => $request->getSchemeAndHttpHost() . '/' . 'storage/' . $pathName . '/' . $file,
                    'type'       => $validate['type'] ?? 'foto'
                ]);
                return response()->json($message, 200);
            } else {
                $teacher = $teachers->save();
                if (!$teacher) throw new \App\Exceptions\ErrorException("Failed insert data.!");
                return response()->json($message, 200);
            }
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if (empty($request->id)) throw new \App\Exceptions\ErrorException("Required data selected.!");

        if (gettype($request->id) == 'array') {
            $teacher  = Teacher::select('id')->whereIn('code_teachers', $request->id)->get();
            $teacher->map(function ($list) {
                return $list->id;
            });
            $teacher = $teacher->toArray();
        } else {
            $teacher = Teacher::where(['code_teachers' => $request->id])->first();
            $teacher = (array)$teacher->id;
        }
        try {
            $delete     = self::DeletedData(Teacher::class, $request->params, $teacher);
            if (!$delete) throw new \App\Exceptions\ErrorException("Failed deleted data.!");

            return response()->json([
                'message'   =>  'Delete data successfully.!',
                'table'     => 'table-teachers'
            ], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }
}
