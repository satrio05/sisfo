<?php

namespace App\Http\Controllers\Admin\Books;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
    }
    public function booksRole()
    {
        $formPublisher = $this->FormColumns([
            ['name', 'text', 'fa fa-text-width'], ['company_name', 'text', 'fa fa-text-width'], ['phone_number', 'number', 'fa fa-sort-numeric-down'], ['location', 'textarea', 'fa fa-map-marker']
        ]);
        $formTypesRole = $this->FormColumns([
            ['name', 'text', 'fa fa-text-width', '', 'name', 'md']
        ]);
        return view('admin.books.role-books')->with(['ColumnsForm' => ['publisher' => $formPublisher, 'types' => $formTypesRole]]);
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->input('changeStatus')) {
            $data = 'a';
        } else {
            $validate = $request->validated();
            if ($request->only('types')) {
                $data = [];
                foreach ($validate['name'] as $key => $value) {
                    $data[] = [
                        'slug'   => \Illuminate\Support\Str::slug($validate['slug'][$key] ?? $value),
                        'name'   => $validate['name'][$key],
                        'status' => $validate['status'][$key] ?? '0',
                        'since'  => $validate['since'][$key]
                    ];
                }
            } else {
                $data = [
                    'name'          => $validate['name'],
                    'company_name'  => $validate['company_name'],
                    'location'      => $validate['location'],
                    'phone_number'  => $validate['phone_number']
                ];
            }
        }
        try {
            if ($request->only('types'))
                $Classes = \App\Models\Master\Publisher::insert($data);
            else
                $Classes = \App\Models\Master\Publisher::create($data);
        } catch (QueryException $th) {
            return response(['errors'    => __('Duplicate')], 422)->header('Content-Type', 'text/plain');
        }
        return response()->json(['message' => __('Insert data successfully.!'), 'table' => 'table-first'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
