<?php

namespace App\Http\Controllers\Admin\Books;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Books\{ListBooksAdd, ListBooksUpdate};
use App\Models\Master\ListBooks;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Database\QueryException;
use App\Helpers\DeleteMethod;

class ListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ColumnsForm = $this->FormColumns([
            ['name_books', 'text', 'fa fa-text-width', '', 'Entry your name books'],
            ['publisher_id', 'select', 'fa fa-check', 'no-label', 'books/publisher/options', '', 'md', '', 'Publisher'],
            ['totals_page', 'number', 'fa fa-sort-numeric-down', null, 'Entry totals page this book'],
            ['types_id', 'select', 'fa fa-check', 'no-label', 'types/options', '', 'md', 1, 'Type categories book'],
            ['writer', 'text', 'fa fa-pen', '', 'Entry your writer this books']
        ]);
        return view('admin.books.list-books', compact('ColumnsForm'));
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess)
                $model = \App\Models\Master\ListBooks::with(['publisher', 'type'])->select('list_books.*')->orderBy('list_books.id', 'DESC');
            return Datatables::eloquent($model)
                ->addIndexColumn()

                ->addColumn(
                    'publisher_name',
                    function ($row) {
                        return $row->publisher->name;
                    }
                )
                ->addColumn(
                    'type_name',
                    function ($row) {
                        return $row->type->name;
                    }
                )
                ->removeColumn(['id', 'publisher', 'type'])
                ->escapeColumns(['name_books', 'writer', 'totals_page', 'publisher_name', 'type_name'])
                ->addColumn('action', function ($row) use ($userAccess) {
                    $btn = '<button class="btn btn-info btn-sm mr-2" id="info" data-route="list-books/show"  data-id="' . $row->slug . '"><i class="fa fa-info mr-2"></i>Info</button>';
                    $btn = $btn . '<button class="btn btn-warning btn-sm mr-2" id="edit" data-route="list-books/show" data-name="list-books-update"  data-id="' . $row->slug . '" ><i class="fa fa-pen mr-2"></i>Edit</button>';
                    $btn = $btn . '<button class="delete btn btn-danger btn-sm" data-route="../books/list-books/destroy" data-id="' . $row->slug . '" method-delete="soft"><i class="fa fa-trash mr-2"></i>Delete</button>';
                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action', 'publisher_name', 'type_name'])

                ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ListBooksAdd $request)
    {
        $validate = $request->validated();
        $validate['slug'] = \Illuminate\Support\Str::slug($validate['name_books'] . ' ' . $validate['writer']);
        try {
            $insert = ListBooks::insert($validate);
            return response()->json([
                'message'   =>  'Created data successfully.!',
                'table'     => 'table-list-books'
            ]);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(listBooks $listBooks)
    {
        if (empty($listBooks))
            throw new \App\Exceptions\ErrorException("Data not found.!");
        else {
            $message = [
                'text' => [
                    'name_books' => $listBooks->name_books,
                    'writer'     => $listBooks->writer,
                ],
                'number'             =>
                [
                    'totals_page'    => $listBooks->totals_page
                ],
                'selectAutocomplate' => [
                    'publisher_id'  =>  ['id' => $listBooks->publisher_id, 'name' => $listBooks->publisher->name],
                    'types_id'      => ['id' => $listBooks->types_id, 'name' => $listBooks->type->name]
                ]
            ];
            return response()->json(['message' => $message], 200);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ListBooksUpdate $request, ListBooks $listBooks)
    {
        $validate = $request->validated();

        $validate['slug'] = \Illuminate\Support\Str::slug($validate['name_books'] . ' ' . $validate['writer']);
        try {
            $update = ListBooks::where(['id' => $listBooks->id])->update($validate);
            if (!$update) throw new \App\Exceptions\ErrorException("Failed updated data.!");
            return response()->json([
                'message'   =>  'Updated data successfully.!',
                'table'     => 'table-list-books'
            ]);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }

    public function destroy(Request $request)
    {
        if (empty($request->id)) throw new \App\Exceptions\ErrorException("Required minimal 1 data.!");
        if (gettype($request->id) == 'array') {
            $listBooks  = ListBooks::select('id')->whereIn('slug', $request->id)->get();
            $listBooks->map(function ($list) {
                return $list->id;
            });
            $listBooks = $listBooks->toArray();
        } else {
            $listBooks = ListBooks::where(['slug' => $request->id])->first();
            $listBooks = (array)$listBooks->id;
        }
        try {
            $delete     = DeleteMethod::DeletedData(ListBooks::class, $request->params, $listBooks);
            if (!$delete) throw new \App\Exceptions\ErrorException("Failed deleted data.!");

            return response()->json([
                'message'   =>  'Delete data successfully.!',
                'table'     => 'table-list-books'
            ]);
        } catch (QueryException $th) {
            throw new \App\Exceptions\DatabaseException($th->getCode());
        }
    }
}
