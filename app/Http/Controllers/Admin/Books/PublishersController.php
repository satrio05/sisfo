<?php

namespace App\Http\Controllers\Admin\Books;

use \App\Http\Controllers\Controller;
use App\Models\Master\Publisher;
use Illuminate\Http\Request;
use DataTables;

use Illuminate\Database\QueryException;

class PublishersController extends Controller
{

    public static function index()
    {

        // return view('admin.books.publisher');
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess)
                $model = \App\Models\Master\Publisher::latest();
            return Datatables::eloquent($model)
                ->addIndexColumn()
                ->editColumn('phone_number', function ($row) {
                    return \App\Models\Master\Publisher::PhoneNumberFormated($row->phone_number);
                })
                ->escapeColumns(['name', 'location', 'company_name', 'phone_number'])
                ->skipTotalRecords()
                ->addColumn('action', function ($row) use ($userAccess) {
                    $btn = '<button class="btn btn-info btn-sm mr-2" id="info" data-route="publisher-info"  data-id="' . $row->id . '"><i class="fa fa-info mr-2"></i>Info</button>';
                    $btn = $btn . '<button class="btn btn-warning btn-sm mr-2" id="edit" data-route="publisher-info" data-name="publisher-update"  data-id="' . $row->id . '" ><i class="fa fa-pen mr-2"></i>Edit</button>';

                    $btn = $btn . '<button class="delete btn btn-danger btn-sm" data-route="publishers-destroy" data-id="' . $row->id . '" ><i class="fa fa-trash mr-2"></i>Delete</button>';
                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action'])

                ->toJson();
        }
    }


    public function options(Request $request)
    {
        $data = [];
        $term  = trim($request->search);
        if (empty($term)) return response()->json($data);
        // $model = Publisher::latest()->get();
        if (!empty($term)) {
            $model = Publisher::where('name', 'like', '%' . $term . '%')->get();
            foreach ($model as $values) {
                $data[] = ['id' => $values->id, 'name' => $values->name];
            }
            return response()->json($data);
        }
    }

    public function create()
    {
        //
    }


    public function store(\App\Http\Requests\Admin\Books\PublisherRequest $request)
    {
        if ($request->input('changeStatus')) {
            $data = 'a';
        } else {
            $validate = $request->validated();
            if ($request->only('types')) {
                $data = [];
                foreach ($validate['name'] as $key => $value) {
                    $data[] = [
                        'name'          => $value,
                        'company_name'  => $validate['company_name'][$key],
                        'location'      => $validate['location'][$key],
                        'phone_number'  => $validate['phone_number'][$key]
                    ];
                }
            } else {
                $data = [
                    'name'          => $validate['name'],
                    'company_name'  => $validate['company_name'],
                    'location'      => $validate['location'],
                    'phone_number'  => $validate['phone_number']
                ];
            }
        }
        try {
            if ($request->only('types'))
                $Classes = \App\Models\Master\Publisher::insert($data);
            else
                $Classes = \App\Models\Master\Publisher::create($data);
        } catch (QueryException $th) {
            return response(['errors'    => __('Insert data failed.!')], 422)->header('Content-Type', 'text/plain');
        }
        return response()->json(['message' => __('Insert data successfully.!'), 'table' => 'table-publishers'], 200);
    }


    public function show(\App\Models\Master\Publisher $publisher)
    {
        $message = [
            'text'  => [
                'name'          => $publisher->name,
                'company_name'  => $publisher->company_name,
                'phone_number'  => $publisher->phone_number,
            ],
            'textarea'  => [
                'location'      => $publisher->location
            ]
        ];
        return response()->json(['message' => $message], 200);
    }

    public function edit($id)
    {
        //
    }


    public function update(\App\Http\Requests\Admin\Books\PublisherUpdateRequest $request, \App\Models\Master\Publisher $publisher)
    {
        $validate = $request->validated();
        if ($request->input('changeStatus'))
            $data =  'a';
        else
            $data = [
                'name'          => $validate['name'],
                'company_name'  => $validate['company_name'],
                'location'      => $validate['location'],
                'phone_number'  => $validate['phone_number']
            ];
        $publisher = $publisher->where(['id' => $publisher->id])->update($data);
        if (!$publisher)  return response(['errors'    => __('Updated data failed.!')], 302)->header('Content-Type', 'text/plain');
        return response()->json(['message' => __('Updated data successfully.!'), 'table' => 'table-publishers'], 200);
    }

    public function destroy(Request $request)
    {
        if ($request->id === 'all-destroy') {
            $Classes = \App\Models\Master\Publisher::truncate();
        } else {
            $validate = $request->validate(['id' => 'required']);
            if (is_numeric($request->id))
                $Classes = \App\Models\Master\Publisher::find($request->id);
            if (is_array($request->id)) {
                $Classes = \App\Models\Master\Publisher::whereIn('id', $request->id);
            }
            if (!$Classes) throw new \App\Exceptions\ErrorException("Data not found.!");
        }
        try {
            if ($request->id == 'all-destroy')
                $Classes = $Classes;
            else
                $Classes->delete();
            return response()->json(['message' => __('Deleted data successfully.!'), 'table' => 'table-publishers'], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\ErrorException('Deleted data failed.!');
        }
    }
}
