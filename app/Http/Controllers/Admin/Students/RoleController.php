<?php

namespace App\Http\Controllers\Admin\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Class = $this->FormColumns([
            ['slug', 'text', 'fa fa-text-width', '', 'Slug'], ['name', 'text', 'fa fa-text-width', '', 'Name'],

            ['status', 'select', 'fa fa-text-width', 'no-label', '', ['0' => 'non active', '1' => 'active']],
            ['since', 'number', 'fa  fa-sort-numeric-down', '', 'Since']
        ]);
        $formTypesRole = $this->FormColumns([
            ['name', 'text', 'fa fa-text-width', '', 'name', 'md']
        ]);
        $formTypesScore = $this->FormColumns([
            ['grade', 'text', 'fa fa-text-width', '', 'grade', 'md'],
            ['score', 'number', 'fa fa-sort-numeric-down', '', 'score max 100', 'md']
        ]);
        return view('admin.students.role')->with(['ColumnsForm' => ['Classes' => $Class, 'types' => $formTypesRole, 'grade-score' => $formTypesScore]]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
