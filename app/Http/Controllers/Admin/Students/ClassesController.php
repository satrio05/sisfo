<?php

namespace App\Http\Controllers\Admin\Students;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Database\QueryException;
use Excel;
use App\Exports\Admin\ClassesFormat;
use App\Http\Requests\Admin\ExportFilter;
use App\Imports\Admin\ClassesImport;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $ColumnsForm = $this->FormColumns([
        //     ['slug', 'text', 'fa fa-text-width'], ['name', 'text', 'fa fa-text-width'], ['status', 'select', 'fa fa-text-width', ['0' => 'non active', '1' => 'active']], ['since', 'number', 'fa  fa-fw fa-sort-numeric-down']
        // ]);
        // $RequiredImport = ['File extension excel or csv', 'Maximal size 10 mb', 'Statuses use 0 => non active and 1 => active', [
        //     'Download format' => ['icon' => 'fa fa-file-download', 'route' => 'classes-export']
        // ]];

        // return view('admin.classes.index')->with(['ColumnsForm' => $ColumnsForm]);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess)
                $model = \App\Models\Admin\Classes::query()->where(['status' => '1'])->latest();
            return Datatables::eloquent($model)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $status = $row->status == '1' ? '<label class="badge badge-success">Active</label>' : '<label class="badge badge-danger">No Active</label>';
                    return $status;
                })
                ->removeColumn(['id'])
                ->escapeColumns(['slug', 'name', 'since'])
                ->skipTotalRecords()
                ->addColumn('action', function ($row) use ($userAccess) {
                    $btn = '<button class="btn btn-info btn-sm mr-2" id="info" data-route="classes-show"  data-id="' . $row->slug . '"><i class="fa fa-info mr-2"></i>Info</button>';
                    $btn = $btn . '<button class="btn btn-warning btn-sm mr-2" id="edit" data-route="classes-show" data-name="classes-update"  data-id="' . $row->slug . '" ><i class="fa fa-pen mr-2"></i>Edit</button>';
                    $btn = $btn . '<button class="delete btn btn-danger btn-sm" data-route="classes-destroy" data-id="' . $row->id . '" ><i class="fa fa-trash mr-2"></i>Delete</button>';
                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action'])

                ->toJson();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // \App\Http\Requests\Admin\Classes\AddRequest
    public function store(\App\Http\Requests\Admin\Classes\AddRequest $request)
    {
        // ddd($val)
        if ($request->input('changeStatus')) {
            $data = 'a';
        } else {
            $validate = $request->validated();
            if ($request->only('types')) {
                $data = [];
                foreach ($validate['name'] as $key => $value) {
                    $data[] = [
                        'slug'   => \Illuminate\Support\Str::slug($validate['slug'][$key] ?? $value),
                        'name'   => $validate['name'][$key],
                        'status' => $validate['status'][$key] ?? '0',
                        'since'  => $validate['since'][$key]
                    ];
                }
            } else {
                $data = [
                    'slug'  => \Illuminate\Support\Str::slug($validate['slug'] ?? $validate['name']),
                    'name'  => $validate['name'],
                    'since'  => $validate['since'],
                    'status' => $validate['status'] ?? '0'
                ];
            }
        }
        try {
            if ($request->only('types'))
                $Classes = \App\Models\Admin\Classes::insert($data);
            else
                $Classes = \App\Models\Admin\Classes::create($data);
        } catch (QueryException $th) {
            return response(['errors'    => __('Duplicate')], 422)->header('Content-Type', 'text/plain');
        }
        return response()->json(['message' => __('Insert data successfully.!'), 'table' => 'table-classes'], 200);
    }

    public function show(\App\Models\Admin\Classes $classes)
    {
        $message = [
            'text'  => [
                'name' => $classes->name,
                'slug'  => $classes->slug,
                'since' => $classes->since
            ],
            'select'  => ['status' => $classes->status]
        ];
        return response()->json(['message' => $message], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(\App\Http\Requests\Admin\Classes\UpdateRequest $request, \App\Models\Admin\Classes $classes)
    {
        $validate = $request->validated();
        if ($request->input('changeStatus'))
            $data =  'a';
        else
            $data = [
                'slug'      => \Illuminate\Support\Str::slug($validate['slug'] ?? $validate['name']),
                'name'      => $validate['name'],
                'since'     => $validate['since'],
                'status'    => $validate['status'] ?? '0'
            ];
        $Classes = $classes->where(['id' => $classes->id])->update($data);
        if (!$Classes)  return response(['errors'    => __('Updated data failed.!')], 302)->header('Content-Type', 'text/plain');
        return response()->json(['message' => __('Updated data successfully.!'), 'table' => 'table-classes'], 200);
        // return response()->json($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        if ($request->id === 'all-destroy') {
            $Classes = \App\Models\Admin\Classes::where('status', '=', '1');
        } else {
            $validate = $request->validate(['id' => 'required']);
            if (is_numeric($request->id))
                $Classes = \App\Models\Admin\Classes::find($request->id);
            if (is_array($request->id)) {
                $Classes = \App\Models\Admin\Classes::whereIn('slug', $request->id);
            }
            if (!$Classes) throw new \App\Exceptions\ErrorException("Data not found.!");
        }
        try {
            $Classes->update(['status' => '0']);
            return response()->json(['message' => __('Deleted data successfully.!'), 'table' => 'table-classes'], 200);
        } catch (QueryException $th) {
            return response(['errors'    => __('Deleted data failed.!')], 422);
        }
    }

    public function import(\App\Http\Requests\Admin\Classes\ImportRequest $request)
    {
        $validate = $request->validated();
        try {
            Excel::import(new ClassesImport, $validate['file']);
            return response()->json(['message' => 'Import data successfully.!', 'table' => 'table-classes'], 200);
        } catch (\Maatwebsite\Excel\Validators\ValidationException $e) {
            $failures = $e->failures();
            return response(['errors'    => __($e->getCode())], 422)->header('Content-Type', 'text/plain');
        }
    }

    public function export(Request $request)
    {
        if ($request->only('id'))
            $filter = ['types' => $request->id];
        else {
            $validate = $request->validate(
                [
                    'start'  => 'required|date',
                    'end'    => 'required|date'
                ]
            );
            $filter = [
                'types' => 'downloads',
                'start' => \Carbon\Carbon::parse($request->start)->format('Y-m-d'),
                'end'   => \Carbon\Carbon::parse($request->end)->format('Y-m-d')
            ];
        }

        $myFile =  Excel::raw(new ClassesFormat($filter), \Maatwebsite\Excel\Excel::XLSX);
        $response =  array(
            'name' => $request->only('id') ? "format-import.xlsx" : 'classes.xlsx',
            'file' => "data:application/vnd.ms-excel;base64," . base64_encode($myFile)
        );
        return response()->json($response);
    }
}
