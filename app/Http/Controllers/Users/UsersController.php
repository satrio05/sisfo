<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DataTables;

class UsersController extends Controller
{
    public function index()
    {
        // $model = \App\Models\Master\Role::with(['users'])->where('roles.name', '=', 'admin')->get();
        $model = \App\Models\User::with('roles')
            ->whereHas('roles', function ($query) {
                $query->where('name', '=', 'admin');
            })
            ->get();
    }

    public function options(Request $request)
    {
        $term  = trim($request->search);
        if (empty($term)) return response()->json([]);
        if (!empty($term)) {
            $data = [];
            $model = \App\Models\User::where('name', 'like', '%' . $term . '%')->where(['statuses_account' => 0])->whereNULL('roles_id')->get();
            foreach ($model as $values) {
                $data[] = ['id' => $values->id, 'name' => $values->name];
            }
            return response()->json($data);
        }
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess) {
                $model = \App\Models\User::with(['roles'])->latest();
            }
            $datatables = DataTables::of($model);
            $datatables->addIndexColumn();
            $datatables->filter(function ($query) use ($request) {
                if ($request->has('filters')) {
                    $query->whereHas('roles', function ($query) use ($request) {
                        $query->where('name', '=', $request->filters);
                    });
                }
            });
            if ($request->filters) {
                $datatables->removeColumn(['name', 'email_verified_at', 'password', 'id', 'roles.id']);
                $datatables->editColumn(
                    'created',
                    function ($row) {
                        return \Carbon\Carbon::parse($row->created_at)->format('M Y');
                    }
                );
                $datatables->editColumn(
                    'status',
                    function ($row) {
                        $status = $row->statuses_account == '1' ? '<label class="badge badge-success">Active</label>' : '<label class="badge badge-danger">No Active</label>';
                        return $status;
                    }
                );

                $datatables->blacklist(['password', 'name', 'id', 'roles.id'])->rawColumns(['status']);
            }
            return $datatables->make(true);

            // ->toJson();
            // $datatables = DataTables::of($model);
            // if ($request->has('filters') {
            //     $model->whereHas('roles',function($query) use ($request){
            //         $query->where('name','=',$request->filters);
            //     })
            // }

            // $datatables =  $datatables->editColumn('created_at', function($row){
            //     return \Carbon\Carbon::parse($row->created_at)->format('M Y');
            //     // })
            // }
            // $datatables->escapeColumns(['email']);
            //     $datatables->editColumn('created_at', function ($row) {
            //         return \Carbon\Carbon::parse($row->created_at)->format('M Y');
            //     });
            // }
            // return $datatables->make(true);
        }
    }

    public function columns()
    {
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        //
    }


    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
