<?php

namespace App\Http\Controllers\Users;

use App\Http\Controllers\Controller;
use App\Http\Requests\Users\AddRoleRequest;
use App\Http\Requests\Users\UpdateRoleRequest;
use App\Models\Master\Role;
use Illuminate\Http\Request;
use DataTables;
use Illuminate\Database\QueryException;

class RolesController extends Controller
{
    public function index()
    {
        $ColumnsForm = $this->FormColumns([
            ['name', 'text', 'fa fa-text-width', '', 'Entry your new role'],
            ['users', 'select', 'fa fa-text-width', 'multi', 'users/options', '', '', 'md', 'Users']
        ]);
        return view('admin.role.index')->with(['form_role' => $ColumnsForm]);
    }

    public function data(Request $request)
    {
        if ($request->ajax()) {
            $user = Auth()->user();
            $userAccess = $user->roles_id == 1 && $user->roles->status == '1';
            if ($userAccess)
                $model = \App\Models\Master\Role::withCount('users')->latest();
            return Datatables::eloquent($model)
                ->addIndexColumn()
                ->editColumn('status', function ($row) {
                    $status = $row->status == '1' ? '<label class="badge badge-success">Active</label>' : '<label class="badge badge-danger">No Active</label>';
                    return $status;
                })
                ->editColumn('users_count', function ($row) {
                    return '<button class="btn btn-info btn-sm mr-2"  id="show" data-route="role/show"  data-id="' . $row->name . '" data-table="users">' . $row->users_count . '</button>';
                })
                ->removeColumn(['id'])
                ->escapeColumns(['name', 'users_count'])
                ->skipTotalRecords()
                ->addColumn('action', function ($row) use ($userAccess) {
                    $btn = '<button class="btn btn-warning btn-sm mr-2" id="edit" data-route="role/show" data-name="/users/role/update/' . $row->name . '" button-edit="hidden" data-id="' . $row->name . '" ><i class="fa fa-pen mr-2"></i>Update</button>';
                    $btn = $btn . '<button class="delete btn btn-danger btn-sm" data-route="role/destroy" data-id="' . $row->id . '" ><i class="fa fa-trash mr-2"></i>Delete</button>';

                    if ($userAccess)
                        return $btn;
                })
                ->rawColumns(['action', 'users_count'])

                ->toJson();
        }
    }

    public function create()
    {
        //
    }

    public function store(AddRoleRequest $request)
    {
        $validate = $request->validated();
        try {
            $role = new Role();
            $role->name = $validate['name'];
            $role->status = '1';
            $role->save();
            if (!$role) throw new \App\Exceptions\ErrorException("Data failed insert.!");
            if ($request->only('users')) {
                $roleId = $role->id;
                $user = \App\Models\User::whereIn('id', $request->users)->update(['roles_id' => $roleId]);
            }
            return response()->json(['message' => __('Insert data successfully.!'), 'table' => 'table-users-role'], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\ErrorException($th->getCode());
        }
    }


    public function show(Role $role)
    {
        if (count($role->users) > 0)
            foreach ($role->users as $value) {
                $users[] = ['id' => $value->id, 'name' => $value->name];
            }
        else $users = [];
        return response()->json([
            'message' => [
                'text'  => [
                    'name'  => $role['name'],
                    'id'    => $role['id']
                ],
                'selectAutocomplate' =>
                [
                    'users' => $users
                ]
            ]
        ]);
    }

    public function edit($id)
    {
        //
    }

    public function update(UpdateRoleRequest $request, Role $role)
    {
        $validate = $request->validated();
        $this->authorize('update', $role);
        if ($request->has('users')) {
            $old    = \App\Models\User::where(['roles_id' => $role->id])->get()->map(function ($old) {
                return $old->id;
            });
            if (count($old) < count($validate['users'])) {
                $Users = array_diff($validate['users'], $old->toArray());
            }
            if (count($old) > count($validate['users'])) {
                $Users = array_diff($old->toArray(), $validate['users']);
            }
            $Users = \App\Models\User::whereIn('id', $Users)->update(['roles_id' => null]);
            if (!$Users) throw new \App\Exceptions\ErrorException("Failed updated new users.!");
        }
        try {
            if (empty($request->users)) $Users = \App\Models\User::where(['roles_id' => $role->id])->update(['roles_id' => null]);
            $role->name = $validate['name'];
            $role->update(['name' => $validate['name']]);
            if (!$role) throw new  \App\Exceptions\ErrorException("Updated data failed.!");
            return response()->json([
                'message' => __('Updated data successfully.!'), 'table' => 'table-users-role'
            ], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\ErrorException($th->getMessage());
        }
    }

    public function destroy(Request $request)
    {
        $role = Role::find($request->id);
        $this->authorize('delete', $role);
        if (!$role) throw new \App\Exceptions\ErrorException("Data not found.!");
        try {
            $role->delete();
            return response()->json(['message' => __('Successfully deleted data.!'), 'table' => 'table-users-role'], 200);
        } catch (QueryException $th) {
            throw new \App\Exceptions\ErrorException($th->getMessage());
        }
    }
}
