<?php

namespace App\Policies\Users;

use App\Models\Master\Role;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function viewAny(User $user)
    {
        // return $user->roles_id === 1;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Role $Role)
    {
        //
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Role $Role)
    {
        return $user->roles_id === 1 ? \Illuminate\Auth\Access\Response::allow() :
            throw new \App\Exceptions\ErrorException("Access failed");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Role $Role)
    {
        return $user->roles_id === 1 && $Role->users()->count() === 0 ? \Illuminate\Auth\Access\Response::allow() :
            throw new \App\Exceptions\ErrorException("Access failed");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function restore(User $user, Role $Role)
    {
        //
    }

    /**
     * Determine whether the user can permanently delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Role  $Role
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function forceDelete(User $user, Role $Role)
    {
        //
    }
}
