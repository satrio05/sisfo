<?php

namespace App\Exports\Admin;

use Maatwebsite\Excel\Concerns\{
    FromCollection,
    WithHeadings,
    WithTitle,
    ShouldAutoSize,
    WithEvents,
};
use Maatwebsite\Excel\Events\AfterSheet;

class ClassesFormat implements
    FromCollection,
    Withheadings,
    WithTitle,
    ShouldAutoSize,
    WithEvents
{

    public $types;
    public $start;
    public $end;

    public function __construct($request)
    {
        foreach ($request as $key => $values) {
            $this->$key = $values;
        }
    }

    public function title(): string
    {
        if ($this->types == 'download-format')
            return __('Format import data');
        else if ($this->types == 'downloads')
            return __('Data classes in ') . ' ' . $this->start . ' to ' . $this->end;
    }

    public function headings(): array
    {
        $columns  =  [
            'no',
            'name',
            // 'slug',
            'status',
            'year'
        ];
        return $columns;
    }

    public function collection()
    {
        $classes = [];
        if ($this->types == 'download-format') {
            $lastCode = \App\Models\Admin\Classes::latest('id')->first();
            $classes[] = [$lastCode->id, 'ex : new class', 'ex: 0', 'ex: 2021'];
        } else {
            $data = \App\Models\Admin\Classes::select('name', 'slug', 'status', 'since')->whereBetween('created_at', [$this->start, $this->end])->get();
            $no = 1;
            foreach ($data as $key) {
                $classes[] = [$no++, $key['name'], $key['slug'], $key['status'], $key['since']];
            }
        }
        $classes = $classes;
        return collect($classes);
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A1:W1')->getFont('Arial')->setSize(12)->setBold(true);
            },
        ];
    }
}
