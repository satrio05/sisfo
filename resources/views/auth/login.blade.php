@extends('adminlte::auth.auth-page', ['auth_type' => 'login'])

@php($login_url = View::getSection('login_url') ?? config('adminlte.login_url', 'login'))

@if (config('adminlte.use_route_url', false))
    @php($login_url = $login_url ? route($login_url) : '')
@else
    @php($login_url = $login_url ? url($login_url) : '')
@endif

@section('auth_header', __('adminlte::adminlte.login_message'))

@section('auth_body')
    <form action="{{ $login_url }}" method="post" id="form">
        @csrf

        {{-- Email field --}}
        <div class="input-group mb-3">
            <input type="email" name="email" class="form-control @error('email') is-invalid @enderror"
                value="{{ old('email') }}" placeholder="{{ __('adminlte::adminlte.email') }}" autofocus>

            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-envelope {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
        </div>

        {{-- Password field --}}
        <div class="input-group mb-3">
            <input type="password" name="password" class="form-control @error('password') is-invalid @enderror"
                placeholder="{{ __('adminlte::adminlte.password') }}">

            <div class="input-group-append">
                <div class="input-group-text">
                    <span class="fas fa-lock {{ config('adminlte.classes_auth_icon', '') }}"></span>
                </div>
            </div>
        </div>

        {{-- Login field --}}
        <div class="row">
            <div class="col-12">
                <button type=submit
                    class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}"
                    id="auth">
                    <span class="fas fa-sign-in-alt"></span>
                    {{ __('adminlte::adminlte.sign_in') }}
                </button>
            </div>
        </div>
    </form>
@stop
@push('js')
    <x-notification></x-notification>
    <script>
        $(document).ready(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            })
            $('button[id="auth"]').on('click', function(e) {
                e.preventDefault()
                let form = $(this).parent().parent().parent()
                $.ajax({
                        url: form.attr('action'),
                        data: form.serialize(),
                        type: 'POST',
                        proccessData: false,
                    }).done(function() {
                        Toast.fire({
                            icon: 'success',
                            title: "{{ __('Please wait') }}"
                        }).then((data) => {
                            window.location.href = "{{ route('home') }}";
                        })

                    })
                    .fail(function(xhr, status, error, responseJSON) {
                        let errors = xhr.responseJSON.errors
                        let message = '';
                        $.each(errors, function(value, index) {
                            message += `${errors[value]} \n`;
                        })
                        Toast.fire({
                            icon: 'error',
                            title: message
                        })
                    })
            })
        })
    </script>
@endPush
