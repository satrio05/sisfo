@extends('adminlte::page')
@section('plugins.Select2', true)
@php
$page = __('Users role');
$config = ['type' => 'Form', 'role' => 'Role'];
@endphp
@section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
    </div>
@stop

@section('content')

    <div class="row">
        <div class="col-8">
            <div class="card">
                <div class="card-body">
                    <table id='table-{{ Str::kebab($page) }}'
                        class="table table-hover table-bordered table-stripped display" width="100%">
                    </table>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card card-success">
                <div class="card-header">
                    <h3 class="card-title">{{ __('Form add') }} {{ __('role') }}</h3>
                </div>
                <div class="card-body">
                    <form method="POST" id="form-input">
                        @csrf
                        @foreach ($form_role as $columns)
                            <x-form.input :config="$columns"></x-form.input>
                        @endforeach
                        <x-adminlte-button type="submit" id="save-form" class="mr-auto" theme="success"
                            label="{{ __('Save') }}" data-type="form" data-route="role/role-add" />
                        <x-adminlte-button type="submit" id="update-form" class="mr-auto" theme="warning"
                            label="{{ __('Update') }} {{ $page }} " data-type="form" data-route="role/update"
                            style="display: none" />
                        <x-adminlte-button type="button" id="back" class="mr-auto" theme="success"
                            label="{{ __('Cancel') }}" style="display: none" />
                    </form>
                </div>
            </div>
        </div>
    </div>
@stop

@push('js')
    <script>
        let dataSource = ['role/data']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.slug}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "5%"
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                    'name': 'name'
                },
                {
                    'title': "{{ __('Status') }}",
                    'data': 'status',
                    'name': 'status'
                },
                {
                    'title': "{{ __('Totals users') }}",
                    'data': 'users_count',
                    // 'name': 'since',
                    "width": "10%"
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',

                }
            ],

        ];
        let ColumnDefs = [
            [{
                'targets': [0, 1, 4, 5],
                'orderable': false,
                "searchable": false,
            }]

        ]
        let dataFilter = [{
            'filters': ''
        }]
        let searching = [true]

        let tableModalColumns = [{
                'title': "{{ __('Number') }}",
                'data': 'DT_RowIndex',
                "width": "5%"
            },
            {
                'title': "{{ __('email') }}",
                'data': 'email',
                'name': 'email'
            },
            {
                'title': "{{ __('Status') }}",
                'data': 'status',
                'name': 'status'
            },
            {
                'title': "{{ __('Created') }}",
                'data': 'created',
                "width": "20%"
            },
        ]
    </script>
    <x-modal.my-modal :config="$config">
        <div class="card-body">
            <form method="POST" class="form-horizontal" id="form-input">
                <x-form.label :config="$form_role[0]"></x-form.label>
            </form>
            <table id='table-modal' class="table table-hover table-bordered table-stripped display" width="100%">
            </table>
        </div>
    </x-modal.my-modal>
    <x-tools-table />
@endpush
