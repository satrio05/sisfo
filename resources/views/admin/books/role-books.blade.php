@extends('adminlte::page')
@section('plugins.Select2', true)
@section('plugins.BsCustomFileInput', true)
@section('plugins.TempusDominusBs4', true)
@php
$import = config('adminlte.import-export.classes');
$page = __('Role-books');
$tabs = [
    [
        'id' => 'publishers',
        'name' => 'publishers-tabs',
        'target' => 'publishers',
        'status' => 'active',
        'action-nav-right' => [
            'add' => [
                'label' => 'Add',
                'type' => 'button',
                'theme' => 'primary',
                'icon' => 'fa fa-fw fa-plus',
                'dropdown' => ['single', 'multiple'],
            ],
            'delete' => [
                'label' => 'Delete',
                'type' => 'button',
                'theme' => 'danger',
                'icon' => 'fa fa-fw fa-trash',
                'dropdown' => ['multiple', 'all'],
            ],
            // 'modal-add' => [
            //     'route' => '/admin/books/publisher-add',
            // ],
        ],
    ],
    [
        'id' => 'types',
        'name' => 'categori-books-tabs',
        'target' => 'categori-books',
        'status' => '',
        'add' => $ColumnsForm['types'],
        'types' => 1,
    ],
];
$dropdownRight = [
    'add' => [
        'label' => 'Add',
        'type' => 'button',
        'theme' => 'primary',
        'icon' => 'fa fa-fw fa-plus',
        'dropdown' => ['single', 'multiple'],
    ],
];
@endphp
@section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <x-tools.nav-tabs-tables :config="$tabs" id="books-tabs">
            </x-tools.nav-tabs-tables>
        </div>
    </div>
@stop
@push('js')
    <script>
        let dataSource = ['../books/books-publishers', '../types/types']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.id}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "5%"
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                    'name': 'name'
                },
                {
                    'title': "{{ __('Company name') }}",
                    'data': 'company_name',
                    'name': 'company_name'
                },
                {
                    'title': "{{ __('Phone number') }}",
                    'data': 'phone_number',
                    'name': 'phone_number'
                },
                {
                    'title': "{{ __('Location') }}",
                    'data': 'location'
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',

                }
            ],
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.id}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "2%"
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                },
                {
                    'title': "{{ __('Action') }}",
                    "data": 'action'
                }
            ],

        ];
        let ColumnDefs = [

            [{
                'targets': [0, 1, 4, 6],
                'orderable': false,
                "searchable": false,
            }],
            [{
                'targets': [0, 1],
                'orderable': false,
                "searchable": false,
            }],
        ]
        let dataFilter = [{
                'filters': ''
            },
            {
                'group': 1
            },
        ]

        let searching = [
            true, false
        ]
    </script>
    <script>
        function adjustTable() {
            $($.fn.dataTable.tables(true)).css('width', '100%')
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw()
        }

        $(document).ready(function() {
            $('a[data-bs-toggle="tab"]').on('click', function(e) {
                $(this).tab('show')
                if (typeof adjustTable !== 'undifined' && typeof adjustTable === 'function') {
                    return adjustTable()
                }
            })
        })
    </script>
    <x-tools-table />
    <x-modal.input :config="$ColumnsForm['publisher']"></x-modal.input>
@endpush
