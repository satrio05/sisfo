@extends('adminlte::page')
@section('plugins.Select2', true)
@section('plugins.TempusDominusBs4', true)
@php
$page = __('List-Books');
$tabs = [
    [
        'id' => 'list_books',
        'name' => 'list-books-tabs',
        'target' => 'list-books-books',
        'status' => 'active',
        // 'add' => $ColumnsForm['types'],
        // 'types' => 1,
    ],
];
$dropdownRight = [
    'add' => [
        'label' => 'Add',
        'type' => 'button',
        'theme' => 'primary',
        'icon' => 'fa fa-fw fa-plus',
        'action' => 'single',
        'route' => 'list-books-add',
        // 'dropdown' => ['single'],
    ],
    'delete' => [
        'label' => 'Delete',
        'type' => 'button',
        'theme' => 'danger',
        'icon' => 'fa fa-fw fa-trash',
        'action' => 'multiple',
        'route' => 'list-books/destroy',
        'methodDelete' => 'soft',
    ],
];
@endphp
@section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
        <div class="ml-auto mb-2">
            <x-form.button classes="list-books" :config="$dropdownRight" />
        </div>
    </div>
@stop
@section('content')
    <div class="card">
        <div class="card-body">
            <table id='table-list-books' class="table table-hover table-bordered table-stripped display" width="100%">
            </table>
        </div>
    </div>
    <x-adminlte-modal id="modalCustom" theme="purple" icon="fa fa-fw fa-plus" size='lg' disable-animations>
        <form method="POST" id="form">
            <div class="row">
                @foreach ($ColumnsForm as $value)
                    <div class="col-md-6">
                        <x-form.input :config="$value"></x-form.input>
                    </div>
                @endforeach
            </div>
        </form>
    </x-adminlte-modal>
@stop
@push('js')
    <script>
        let dataSource = ['../books/list-books/data']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.slug}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "5%"
                },
                {
                    'title': "{{ __('Name Books') }}",
                    'data': 'name_books',
                    'name': 'name_books'
                },
                {
                    'title': "{{ __('Publisher') }}",
                    'data': 'publisher_name',
                    'name': 'publisher.name'
                },
                {
                    'title': "{{ __('Type') }}",
                    'data': 'type_name',
                    'name': 'type.name'
                },
                {
                    'title': "{{ __('Totals Pages') }}",
                    'data': 'totals_page',
                    'name': 'totals_page'
                },
                {
                    'title': "{{ __('Writter') }}",
                    'data': 'writer',
                    'name': 'writer'
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',

                }
            ],
        ];
        let ColumnDefs = [
            [{
                'targets': [0, 1],
                'orderable': false,
                "searchable": false,
            }],
        ]
        let dataFilter = [{
            'filters': ''
        }, ]

        let searching = [
            true
        ]
    </script>
    {{-- <x-modal.input :config="$ColumnsForm"></x-modal.input> --}}
    <x-tools-table />
@endpush
