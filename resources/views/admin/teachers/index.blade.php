@extends('adminlte::page')
@section('plugins.Select2', true)
@section('plugins.BsCustomFileInput', true)
@section('plugins.TempusDominusBs4', true)
@php
$page = __('List Teachers');
$dropdownRight = [
    'add' => [
        'label' => 'Add',
        'type' => 'button',
        'theme' => 'primary',
        'icon' => 'fa fa-fw fa-plus',
        'action' => 'teacher',
        'route' => 'teachers/add',
    ],
];
$tabs = [
    [
        'id' => 'teachers',
        'name' => 'teachers-tabs',
        'target' => 'teachers',
        'status' => 'active',
        'action-nav-right' => [
            'add' => [
                'label' => 'Add',
                'type' => 'button',
                'theme' => 'primary',
                'icon' => 'fa fa-fw fa-plus',
                'dropdown' => ['single', 'upload'],
            ],
            'delete' => [
                'label' => 'Delete',
                'type' => 'button',
                'theme' => 'danger',
                'icon' => 'fa fa-fw fa-trash',
                'dropdown' => ['multiple', 'all'],
            ],
            // 'modal-add' => [
            //     'route' => '/admin/books/publisher-add',
            // ],
        ],
    ],
    [
        'id' => 'teacher-flies',
        'name' => 'teacher-flies-tabs',
        'target' => 'teacher-flies',
        'status' => '',
        'add' => [
            'label' => 'Add',
            'type' => 'button',
            'theme' => 'primary',
            'icon' => 'fa fa-fw fa-plus',
            'dropdown' => ['single', 'multiple'],
        ],
    ],
];
// $dropdownRight = [
//     'add' => [
//         'label' => 'Add',
//         'type' => 'button',
//         'theme' => 'primary',
//         'icon' => 'fa fa-fw fa-plus',
//         'dropdown' => ['single', 'multiple'],
//     ],
// ];
@endphp
{{-- @section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
        <div class="ml-auto mb-2">
            <x-form.button classes="teachers" :config="$dropdownRight" />
        </div>
    </div>
@stop --}}
@section('content')
    {{-- <div class="row"> --}}
    {{-- <div class="col-12"> --}}
    <x-tools.nav-tabs-tables :config="$tabs" id="books-tabs">
    </x-tools.nav-tabs-tables>
    {{-- <table id='table-teachers' class="table table-hover table-bordered table-stripped display" width="100%">
            </table> --}}
    {{-- </div> --}}
    {{-- </div> --}}
    <x-adminlte-modal id="modalCustom" theme="purple" icon="fa fa-fw fa-plus" size='lg' disable-animations>
        <form method="POST" id="form">
            <div class="row">
                @foreach ($ColumnsForm as $value)
                    <div class="col-md-6">
                        @if ($value['name'] !== 'educations' && $value['name'] !== 'name_school' && $value['name'] !== 'accreditation')
                            <x-form.input :config="$value"></x-form.input>
                        @else
                            <x-form.input :config="$value"></x-form.input>
                        @endif
                    </div>
                @endforeach
            </div>
        </form>
    </x-adminlte-modal>
    <x-adminlte-modal id="modalUploadFile" theme="purple" icon="fa fa-fw fa-plus" size='lg' disable-animations>
        <form method="POST" id="form-file">
            <div class="row">
                @foreach ($ColumnsFormUpload as $value)
                    <div class="col-md-6">
                        <x-form.input :config="$value"></x-form.input>
                    </div>
                @endforeach
            </div>
        </form>
    </x-adminlte-modal>

@stop
@push('js')
    <script>
        let dataSource = ['teachers-data', 'teachers/data-files']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.code_teachers}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',
                },
                {
                    'title': "{{ __('Profile') }}",
                    'data': function(row, data) {
                        if (row.profile == 'undifined')
                            return `<button class="btn btn-info btn-sm mr-2" id="upload" data-route="teachers/update" data-id="${row.code_teachers}"><i class="fa fa-plus"></i></button>`
                        else
                            return `<img src="${row.profile}" alt="AdminLTE" class="brand-image img-circle elevation-3" style="opacity:.8" width="50%"">`;
                    },
                    "width": "10%"
                },
                {
                    'title': "{{ __('Code Teacher') }}",
                    'data': 'code_teachers',
                    'name': 'code_teachers',
                    'width': "15%"
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                    'name': 'name'
                },
                {
                    'title': "{{ __('educations') }}",
                    'data': 'educations',
                    'name': 'educations'
                },
                {
                    'title': "{{ __('Status') }}",
                    'data': 'statuses',
                    'name': 'statuses',
                    'width': "10%"
                },
                // {
                //     'title': "{{ __('Address') }}",
                //     'data': 'address',
                //     'name': 'address'
                // },


            ],
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.code_teachers}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',
                },
                // {
                //     'title': "{{ __('Profile') }}",
                //     'data': function(row, data) {
                //         if (row.profile == 'undifined')
                //             return `<button class="btn btn-info btn-sm mr-2" id="upload" data-route="teachers/update" data-id="${row.code_teachers}"><i class="fa fa-plus"></i></button>`
                //         else
                //             return `<img src="${row.profile}" alt="AdminLTE" class="brand-image img-circle elevation-3" style="opacity:.8" width="50%"">`;
                //     },
                //     "width": "10%"
                // },
            ]
        ];
        let ColumnDefs = [
            [{
                'targets': [0, 1, 2, 5, -1],
                'orderable': false,
                "searchable": false,
            }],
            []
        ]
        let dataFilter = [{
            'filters': ''
        }, {
            'filters': ''
        }]

        let searching = [
            true, false
        ]
    </script>

    <script>
        function adjustTable() {
            $($.fn.dataTable.tables(true)).css('width', '100%')
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw()
        }

        $(document).ready(function() {
            $('a[data-bs-toggle="tab"]').on('click', function(e) {
                $(this).tab('show')
                if (typeof adjustTable !== 'undifined' && typeof adjustTable === 'function') {
                    return adjustTable()
                }
            })
        })
    </script>


    <script>
        $(document).ready(function() {
            $('button#teacher-add').on('click', function(e) {
                let route = $(this).data('route')
                console.log(route)
                $('.modal-footer').css('display', '').prepend(`
                        <x-adminlte-button type="submit" id="save" class="mr-auto" theme="success" data-route="${route}" label="Save" icon="fa fa-fw fa-save" />
                        `)
                let size = $(this).data('size')
                $('#modalCustom').modal('show')
                e.preventDefault()
            })

            $('table#table-teachers').on('click', 'button[id="upload"],button#upload-multi-file', function(e) {
                let route = $(this).data('route')
                let id = $(this).data('id')
                e.preventDefault()
                $('.modal-title').html(`<i class="fa fa-edit mr-2"></i> Form add file teacher`)

                if ($(this).attr('id') === 'upload-multi-file') {
                    $('.modal-footer').css('display', '').prepend(`
                                <x-adminlte-button type="submit" id="upload-multi" class="mr-auto" theme="success" data-route="${route}/${id}"  label="Save" icon="fa fa-fw fa-save" />
                                `)
                    $('#modal-multiple-insert').modal('show')
                } else {
                    $('.modal-footer').css('display', '').prepend(`
                        <x-adminlte-button type="submit" id="update-file" class="mr-auto" theme="success" data-route="${route}/${id}"  label="Save" icon="fa fa-fw fa-save" />
                        `)
                    $('#modalUploadFile').modal('show')

                }
            })

            $('#modalUploadFile,#modalCustom').on('hidden.bs.modal', function() {
                $('button[id="save-file"]').remove()
                $('button[id="update-file"],button#multiple-add').remove()
                $('form#form').trigger('reset')
            })

        })
    </script>
    <x-tools-table />
    <x-modal.input :config="$ColumnsFormUpload"></x-modal.input>
@endpush
