@extends('adminlte::page')
@section('plugins.Select2', true)
@section('plugins.BsCustomFileInput', true)
@section('plugins.TempusDominusBs4', true)
@php
$import = config('adminlte.import-export.classes');
$page = __('Classes');
$dropdownRight = [
    'add' => [
        'label' => 'Add',
        'type' => 'button',
        'theme' => 'primary',
        'icon' => 'fa fa-fw fa-plus',
        'dropdown' => ['single', 'multiple'],
    ],
    'delete' => [
        'label' => 'Delete',
        'type' => 'button',
        'theme' => 'danger',
        'icon' => 'fa fa-fw fa-trash',
        'dropdown' => ['multiple', 'all'],
    ],
];
@endphp
@section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
        <div class="ml-auto">
            <x-form.button classes="classes" :config="$dropdownRight" />
            <x-adminlte-button label="Import" id="import" data-route="classes-import" theme="success" name="import"
                icon="fa fa-fw fa-file-import" />
            <x-adminlte-button label="Export" id="export" data-route="classes-export" theme="info" name="export"
                icon="fa fa-fw fa-file-export" />
        </div>
    </div>
    <x-modal.import-export :config="$import"></x-modal.import-export>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id='table-{{ $page }}' class="table table-hover table-bordered table-stripped display"
                        width="100%">
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop
@push('js')
    <script>
        let dataSource = ['classes-data']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.slug}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "5%"
                },
                {
                    'title': "{{ __('Slug') }}",
                    'data': 'slug',
                    'name': 'slug'
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                    'name': 'name'
                },
                {
                    'title': "{{ __('since') }}",
                    'data': 'since',
                    'name': 'since',
                    "width": "10%"
                },
                {
                    'title': "{{ __('Status') }}",
                    'data': 'status',
                    "width": "10%"
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',

                }
            ]
        ];
        let ColumnDefs = [
            [{
                'targets': [0, 1, 5, 6],
                'orderable': false,
                "searchable": false,
                // "bSortable": false,
            }]
        ]
        let dataFilter = [{
            'filters': ''
        }, ]
        let searching = [true]
    </script>
    <x-tools-table />
    <x-modal.input :config="$ColumnsForm"></x-modal.input>
    <x-modal.import-export :config="$import"></x-modal.import-export>
@endpush
