@extends('adminlte::page')
@section('plugins.Select2', true)
@section('plugins.BsCustomFileInput', true)
@section('plugins.TempusDominusBs4', true)
@php
$import = config('adminlte.import-export.classes');
$page = __('Role-students');
$tabs = [
    [
        'id' => 'classes',
        'name' => 'classes-tabs',
        'status' => 'active',
        'target' => 'classes',
        'action-nav-right' => [
            'add' => [
                'label' => 'Add',
                'type' => 'button',
                'theme' => 'primary',
                'icon' => 'fa fa-fw fa-plus',
                'dropdown' => ['single', 'multiple'],
            ],
            'delete' => [
                'label' => 'Delete',
                'type' => 'button',
                'theme' => 'danger',
                'icon' => 'fa fa-fw fa-trash',
                'dropdown' => ['multiple', 'all'],
            ],
            // <x-adminlte-button label="Import" id="import" data-route="classes-import" theme="success" name="import"
            //     icon="fa fa-fw fa-file-import" />
            // <x-adminlte-button label="Export" id="export" data-route="classes-export" theme="info" name="export"
            //     icon="fa fa-fw fa-file-export" />
            'import' => [
                'label' => 'Import',
                'type' => 'button',
                'theme' => 'success',
                'icon' => 'fa fa-fw fa-file-import',
                // 'dropdown' => ['multiple', 'all'],
            ],
            'export' => [
                'label' => 'Export',
                'type' => 'button',
                'theme' => 'warning',
                'icon' => 'fa fa-fw fa-file-download',
            ],
            // 'modal-add' => [
            //     'route' => '/admin/books/publisher-add',
            // ],
        ],
    ],
    [
        'id' => 'types',
        'name' => 'statuses-student-tabs',
        'target' => 'statuses-student',
        'status' => '',
        'add' => $ColumnsForm['types'],
        'types' => 2,
    ],
    [
        'id' => 'types',
        'name' => 'grade-score-student-tabs',
        'target' => 'grade-score-student',
        'status' => '',
        'add' => $ColumnsForm['grade-score'],
        'types' => 3,
    ],
];
@endphp
@section('content_header')
    <div class="d-flex">
        <h1 class="m-0 text-dark">{{ $page }}</h1>
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-12">
            <x-tools.nav-tabs-tables :config="$tabs" id="books-tabs">
            </x-tools.nav-tabs-tables>
        </div>
    </div>
    {{-- <x-adminlte-modal id="modalCustomSingle" theme="purple" icon="fa fa-fw fa-plus" size='lg' disable-animations>
        <form method="POST" id="form">
            <div class="row">
                @foreach ($ColumnsForm['Classes'] as $value)
                    <div class="col-md-6">
                        <x-form.input :config="$value"></x-form.input>
                    </div>
                @endforeach
            </div>
        </form>
    </x-adminlte-modal> --}}
@stop
@push('js')
    <script>
        let dataSource = ['classes-data', '../types/types', '../types/types']
        let tableColumn = [
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.slug}"/>`
                    },
                    "width": "2%"
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "5%"
                },
                {
                    'title': "{{ __('Slug') }}",
                    'data': 'slug',
                    'name': 'slug'
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                    'name': 'name'
                },
                {
                    'title': "{{ __('since') }}",
                    'data': 'since',
                    'name': 'since',
                    "width": "10%"
                },
                {
                    'title': "{{ __('Status') }}",
                    'data': 'status',
                    "width": "10%"
                },
                {
                    'title': "{{ __('Action') }}",
                    'data': 'action',

                }
            ],
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.id}"/>`
                    },
                    "width": "2%",
                    'orderable': false,
                    "searchable": false,
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "2%",
                    'orderable': false,
                    "searchable": false,
                },
                {
                    'title': "{{ __('Name') }}",
                    'data': 'name',
                },
                {
                    'title': "{{ __('Action') }}",
                    "data": 'action',
                    'orderable': false,
                    "searchable": false,

                }
            ],
            [{
                    'data': function(row, data) {
                        return `<input type="checkbox" name="checkid" value="${row.id}"/>`
                    },
                    "width": "2%",
                    'orderable': false,
                    "searchable": false,
                },
                {
                    'title': "{{ __('Number') }}",
                    'data': 'DT_RowIndex',
                    "width": "2%"
                },
                {
                    'title': "{{ __('Grade') }}",
                    'data': 'grade',
                    'orderable': false,
                    "searchable": false,
                },
                {
                    'title': "{{ __('Score') }}",
                    'data': 'score',
                    'orderable': false,
                    "searchable": false,
                },
                {
                    'title': "{{ __('Action') }}",
                    "data": 'action',
                    'orderable': false,
                    "searchable": false,
                }
            ],
        ];
        let ColumnDefs = [
            [{
                'targets': [0, 1, 5, 6],
                'orderable': false,
                "searchable": false,
            }]
        ]
        let dataFilter = [{
                'filters': ''
            },
            {
                'group': 2
            },
            {
                'group': 3
            }
        ]
        let searching = [true, false, false]
    </script>
    <script>
        function adjustTable() {
            $($.fn.dataTable.tables(true)).css('width', '100%')
            $($.fn.dataTable.tables(true)).DataTable().columns.adjust().draw()
        }

        $(document).ready(function() {
            $('a[data-bs-toggle="tab"]').on('click', function(e) {
                $(this).tab('show')
                if (typeof adjustTable !== 'undifined' && typeof adjustTable === 'function') {
                    return adjustTable()
                }
            })
        })
    </script>
    <x-tools-table />
    <x-modal.import-export :config="$import"></x-modal.import-export>
    <x-modal.input :config="$ColumnsForm['Classes']"></x-modal.input>
@endpush
