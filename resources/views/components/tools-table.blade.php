<script>
    $(document).ready(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        })

        let ResetTable = []
        let Tables = []
        $('.table').each(function(i, value) {
            ResetTable.push('#' + $(this).attr('id'))
            if ($(this).attr('id') !== 'multiple-table' && $(this).attr('id') !== 'table-modal') {
                Tables[i] = $('#' + $(this).attr('id')).DataTable({
                    "processing": true,
                    "serverSide": true,
                    "responsive": true,
                    "ordering": true,
                    "info": true,
                    "searching": typeof searching !== 'undifined' ? searching[i] : true,
                    "aaSorting": [],
                    "border-collapse": "collapse",
                    "width": "100%",
                    "ajax": {
                        "url": dataSource[i],
                        "type": "POST",
                        "data": JSON.parse(JSON.stringify(dataFilter[i])),
                        "datatype": "json",
                    },
                    "columns": tableColumn[i],
                    "columnDefs": ColumnDefs[i]
                })
            }
        })
        $(document).on('click',
            'button[id="add"],button[id="single-add"],button[id="import"],button[id="export"],button#all-destroy,button#multiple-destroy,button#multiple-add',
            function(e) {
                e.preventDefault()
                let route = $(this).data('route')
                let tags = $(this).attr('id')
                let methodDelete = $(this).attr('method-delete')
                if (tags == 'import' || tags == 'export') {
                    let modal = $(this).attr('name')
                    if ($(this).attr('id') == 'import') {
                        $('.modal-title').html(
                            `<i class="fa fa-fw fa-file-import fa-fw"></i> Import data ${modal}`)
                        $('.modal-footer').find('#save-import').attr('data-route', route)
                    } else {
                        $('.modal-title').html(
                            `<i class="fa fa-file-export fa-fw"></i> Export data ${modal}`)
                        $('.modal-footer').find('#downloads-export').attr('data-route', route)
                    }
                    $(`#modal-${modal}`).modal('show')

                } else if (tags == 'multiple-add') {
                    $('.modal-footer').hide()
                    $('.modal-title').html(
                        `<i class="fa fa-plus fa-fw"></i>{{ __('Form multiple insert') }}`)
                    $('.fields').css('display', '').append(`
                        <x-adminlte-button type="submit" id="${tags}-insert" class="float-end mr-auto" theme="success" data-route="${route}" label="Save" icon="fa fa-fw fa-save" />
                        `)
                    $(`#modal-multiple-insert`).modal('show')
                } else if (tags == 'all-destroy') {
                    confirmData(route, tags, methodDelete)
                } else if (tags == 'multiple-destroy') {
                    let multiples = []
                    $('input[name="checkid"]:checked').each(function() {
                        multiples.push($(this).val())
                    })
                    confirmData(route, multiples, methodDelete)
                } else if (tags == 'single-add') {
                    $('.modal-title').html(`<i class="fa fa-plus mr-2"></i> Form add data`)
                    $('.modal-footer').css('display', '').prepend(`
                        <x-adminlte-button type="submit" id="save" class="mr-auto" theme="success" data-route="${route}" label="Save" icon="fa fa-fw fa-save" />
                        `)
                    let size = $(this).data('size')
                    $('#modalCustom').modal('show')
                }
            })
        $(ResetTable.toString()).on('click',
            'button.delete,button#edit-form,button#edit,button#info,button#edit-select,button#show,button#update-file',
            function(e) {
                e.preventDefault()
                const url = $(this).data('route')
                const action = $(this).text()
                const option = $(this).attr('id')
                let id = $(this).data('id')
                let methodDelete = $(this).attr('method-delete')
                const editOneClick = $(this).data('select');
                // console.log(action)
                if ($(this).attr('id') === 'show') {
                    $('.modal-title').html(`<i class="fa fa-fw fa-info mr-2"></i> Form Information`)
                    dataForm(url, id, $(this).attr('id'))
                    tableModal($(this).data('table'), id)
                    // tableModal.DataTable().draw(false)
                    $('#newModal').modal('show')
                }
                if (action === 'Delete' || option === 'delete') {
                    confirmData(url, id, methodDelete)
                }
                if (action === 'Info' || action === 'Update' || action === 'Edit' || option == 'info' ||
                    option == 'edit') {

                    if (action === 'Info' || option === 'info') {
                        $('.modal-title').html(`<i class="fa fa-info-circle mr-2"></i> Form Information`)
                        $('.modal-footer').css('display', 'none')
                    } else if (action === 'Update' || option === 'update') {
                        $("#form-input select").val('').change();
                        let route = $(this).data('name')
                        $('button#save-form').css('display', 'none')
                        $('button#update-form').css('display', '')
                        $('button#back').css('display', '')
                        $('form#form-input').attr('action', `${route}`)
                        if ($(this).attr('button-edit') == 'hidden') {
                            // $('select').select2({
                            //     minimumResultsForSearch: -1
                            // })
                        }
                    } else {
                        let route = $(this).data('name')
                        $('.modal-title').html(`<i class="fa fa-edit mr-2"></i> Form edit data`)
                        $('.modal-footer').css('display', '').prepend(
                            `<x-adminlte-button type="submit" id="update" data-route="${route}/${id}" class="mr-auto" theme="warning" label="Save" icon="fa fa-save" />`
                        )
                    }
                    dataForm(url, id, action, option)

                    if (action != 'Update' || option === 'info') {
                        $('#modalCustom').modal('show')
                    }
                }

            })


        $(document).on('click', '#save-import,#downloads-export', function(e) {
            e.preventDefault()
            let url = $(this).data('route')
            let typeFile = $(this).data('name')
            let form = new FormData(document.getElementById(`form-${typeFile}`))
            $.ajax({
                url: url,
                type: 'POST',
                data: form,
                enctype: 'multipart/form-data',
                dataType: "json",
                contentType: false,
                processData: false
            }).done(function(data) {
                if (typeFile == 'export') {
                    var link = document.createElement('a');
                    link.href = data.file;
                    link.download = data.name;
                    document.body.appendChild(link);
                    link.click();
                    document.body.removeChild(link);
                } else {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    })
                    $('#' + data.table).DataTable().draw(false)
                }
            }).fail(function(xhr, status, error, responseJSON) {
                let errors = xhr.responseJSON.errors
                // console.log(errors)
                let message = '';
                if (typeof errors == 'string')
                    message += errors
                else
                    $.each(errors, function(value, index) {
                        message += `${errors[value]} \n`;
                    })
                Toast.fire({
                    icon: 'error',
                    title: message
                })
            })
        })
        $('.modal').on('click',
            'button[id="save"],button[id="update"],button[id="save-file"]',
            function(e) {
                e.preventDefault()
                let url = $(this).data('route')
                let button = $(this)

                let form
                if ($(this).attr('id') === 'update-file') {
                    let formFile = $('form#form-file')[0]
                    form = new FormData(formFile)
                } else
                    form = $('form#form').serialize()
                // // console.log($(this).attr('id'))

                ajaxSend(url, button, form)
            })

        $(document).on('click',
            'button[id="save-form"],button[id="update-form"],button[id="back"],button[id="update-file"],button#upload-multi',
            function(e) {
                e.preventDefault()
                let url = $(this).data('route')
                let button = $(this)
                if (button.attr('id') == 'back') {
                    $(this).closest('form').trigger('reset')
                    $('form').find("select").val('').change();
                    // $(this).css('display', 'none')
                    $('button[id="update-form"]').css('display', 'none')
                    $('button[id="save-form"]').css('display', '')

                } else {
                    // console.log($(this).find('#form-input'))
                    // let form = $(this).closest('form').serialize()
                    // let form
                    if ($(this).attr('id') === 'update-file' || ($(this).attr('id') ===
                            'upload-multi')) {
                        let form
                        if ($(this).attr('id') === 'update-file')
                            form = $('form#form-file').serialize()
                        if ($(this).attr('id') === 'upload-multi')
                            form = $('form#form-multiple').serialize()
                        // let form = new FormData(formFile)
                        ajaxSend(url, button, form)
                    } else {

                        let form = $(this).closest('form').serialize()

                        ajaxSend(url, button, form)
                    }
                }
            })

        $('#form-multiple').on('submit', function(e) {
            e.preventDefault()
            let url = $($(this)).find('#multiple-add-insert')
            let button = url
            url = url.data('route')
            let form = $(this).serialize()
            ajaxSend(url, button, form)
        })

        $('#modalCustom,#newModal,#modal-import,#modal-export, #modal-multiple-insert,#modalUploadFile').on(
            'hidden.bs.modal',
            function() {
                $(this).find('form').trigger('reset');
                // $("#table-modal").dataTable().fnDestroy();
                $("#form-input").trigger("reset")
                // $("#form-multiple").trigger("reset")
                $("#form").trigger("reset")
                // $("#form-import").trigger("reset")
                if ($(this).attr('id') === 'modalCustom' || $(this).attr('id') ===
                    'modal-multiple-insert') {
                    let cacheButton = $(this).find('.modal-footer').children().attr('id')
                    $(`button`).remove(`#${cacheButton}`)
                    console.log(cacheButton)
                }
                $("#form input, select,textarea").removeAttr('readonly disabled')
                $("#form select").val('').change()
                $('.modal-title').empty()
                $('.tag-required').empty()
                $('.fields').empty()
                // $('#form img').removeAttr('src').trigger()
            });

        $(document).on('click', '#download-format', function(e) {
            e.preventDefault()
            $.ajax({
                type: 'POST',
                cache: false,
                url: $(this).data('route'),
                data: {
                    id: $(this).attr('id')
                },
            }).done(function(result, status, xhr) {
                var link = document.createElement('a');
                link.href = result.file;
                link.download = result.name;

                document.body.appendChild(link);

                link.click();
                document.body.removeChild(link);

            })
        })

        $('select').each(function(i, el) {
            let name = $(this).attr('id')
            // console.log(name)
            let route;
            let filter;
            if (typeof name !== "undefined") {
                route = $(this).data('route')
                filter = $(this).data('filter')
            }
            if ($(this).children().length == 0) {
                $(`#${name}`).select2({
                    allowClear: true,
                    theme: 'bootstrap4',
                    placeholder: "{{ __('Search for a data ') }}" + $(this).attr('placeholder'),
                    "ajax": {
                        "datatype": 'JSON',
                        "url": route,
                        "data": function(response) {
                            if (filter !== undefined || filter !== null) {
                                return {
                                    filters: filter,
                                    search: $.trim(response.term)
                                }
                            } else {
                                return {
                                    search: $.trim(response.term)
                                }
                            }
                        },


                        processResults: function(data) {
                            return {
                                results: $.map(data, function(item) {
                                    return {
                                        text: item.name,
                                        id: item.id
                                    }
                                }),
                            };
                        },
                        cache: true
                    },

                })
            }
        })

    })

    function tableModal(url, id) {
        $(document).ready(function() {
            let table = $('#table-modal').DataTable({
                "processing": true,
                "serverSide": true,
                "responsive": true,
                "ordering": false,
                "info": false,
                "bDestroy": true,
                "searching": false,
                "scrollY": '50vh',
                "autoWidth": true,
                "scrollCollapse": true,
                "aaSorting": [],
                "border-collapse": "collapse",
                "width": "100%",
                "ajax": {
                    "url": '/' + url + '/data',
                    "type": "POST",
                    "data": function(data) {
                        data.filters = id
                    },
                    "datatype": "json",
                },
                "columns": tableModalColumns,
            })
            // table.destroy();
        })
    }

    function dataForm(url, id, action, option = null) {
        $.get(`${url}/${id}`, function(data) {
            $.each(data.message.text, function(value, index) {
                if (action === 'Info' || option === 'info')
                    $(`input[name=${value}`).val(index).prop('readonly', true)
                else if (action === 'show')
                    $(`label[for=${value}]`).html(index)
                else
                    $(`input[name=${value}`).val(index)
            })
            $.each(data.message.number, function(value, index) {
                if (action === 'Info' || option === 'info')
                    $(`input[name=${value}`).val(index).prop('readonly', true)
                else if (action === 'show')
                    $(`label[for=${value}]`).html(index)
                else
                    $(`input[name=${value}`).val(index)
            })
            $.each(data.message.textarea, function(value, index) {
                if (action === 'Info' || option === 'info')
                    $(`textarea[name=${value}`).val(index).prop('readonly',
                        true)
                else
                    $(`textarea[name=${value}`).val(index)
            })
            $.each(data.message.select, function(value, index) {
                if (action === 'Info' || option === 'info')
                    $(`select[name="${value}`).val(index).attr('disabled', true)
                    .trigger('change');
                else {
                    $(`select[name="${value}`).val(index).trigger('change');
                }
            })
            $.each(data.message.selectAutocomplate, function(value, index) {
                //add option tag in select
                let val
                if (action === 'Info' || option === 'info') {
                    if (index.length !== undefined) {
                        $.each(index, (key, i) => {
                            val = $("<option selected></option>").val(index[key].id).text(index[
                                key].name)
                            $(`select[id="${value}"]`).append(val).trigger('change');
                        })
                    } else {
                        val = $("<option selected></option>").val(index.id).text(index.name);
                    }
                    $(`select[id="${value}"]`).append(val).trigger('change').attr('disabled', true);
                    // // $(`select[name="${value}`).val(index.id)
                } else {
                    if (index.length !== undefined) {
                        $.each(index, (key, i) => {
                            val = $("<option selected></option>").val(index[key].id).text(index[
                                key].name)
                            $(`select[id="${value}"]`).append(val).trigger('change');
                        })
                    } else {
                        val = $("<option selected></option>").val(index.id).text(index.name)
                        $(`select[id="${value}"]`).append(val).trigger('change');
                    }

                }
            })

            $.each(data.message.file, function(value, index) {
                console.log(index.length)
                if (action === 'Info' || option === 'info') {
                    if (index.length <= 1)
                        $(`div[id="${value}"]`).append(
                            `<img src="${index[0].file}" name="${value}">`)
                    else {
                        $.each(index, (key, i) => {
                            $(`input[name=${value}`).parent().parent().inner(
                                `<img src="${index[key].file}">`)
                        })
                    }
                }
                // else
                // $(`textarea[name=${value}`).val(index)
            })

        })
    }

    function confirmData(url, id, params = null) {
        const SwalConfirm = Swal.fire({
            title: "{{ __('Are you sure?') }}",
            text: "{{ __('You wont be able to revert this!') }}",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: "{{ __('Yes, delete it!') }}"
        }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: url,
                    type: 'DELETE',
                    data: {
                        id: id,
                        params: params ?? null,
                        method: 'DELETE',
                        submit: true
                    }
                }).done(function(data) {
                    Toast.fire({
                        icon: 'success',
                        title: data.message
                    })
                    $('#' + data.table).DataTable().draw(false)
                }).fail(function(xhr, status, error, responseJSON) {
                    let errors = xhr.responseJSON.errors
                    let message = '';
                    $.each(errors, function(value, index) {
                        message += `${errors[value]} \n`;
                    })
                    Toast.fire({
                        icon: 'error',
                        title: message
                    })
                })
            }
        })
    }

    function ajaxSend(url = null, button, form) {
        action = button.attr('id')
        let Send
        if (action == 'update-file' || action == 'upload-multi') {
            let formData
            if (action == 'update-file')
                formData = new FormData($('form#form-file')[0])
            else
                formData = new FormData($('form#form-multiple')[0])
            formData.append('_method', 'PUT')
            Send = $.ajax({
                url: url,
                data: formData,
                type: 'POST',
                enctype: 'multipart/form-data',
                contentType: false,
                processData: false,
                dataType: "json",
            })
        } else {
            Send = $.ajax({
                url: action == 'update-form' ? $('#form-input').attr('action') : url,
                type: action == 'save' || action == 'save-form' || action ==
                    'multiple-add-insert' ?
                    'POST' : 'PUT',
                data: form,
                dataType: "json",
                processData: false,
            })
        }
        Send.done(function(data) {
            Toast.fire({
                icon: 'success',
                title: data.message
            }).then((data) => {
                if (action == 'save' || action == 'save-form' || action == 'update-form' || action ==
                    'multiple-add-insert') {
                    triggerRest()
                }
            })
            $('#' + data.table).DataTable().draw(false)
        }).fail(function(xhr, status, error, responseJSON) {
            let errors = xhr.responseJSON.errors
            let message = ''
            if (typeof errors == 'string')
                message += errors
            else
                $.each(errors, function(value, index) {
                    message += `${errors[value]} \n`;
                })
            Toast.fire({
                icon: 'error',
                title: message
            })
        })
    }



    function triggerRest() {
        $("#form-input").removeAttr("action")
        $("#save-form").css("display", '')
        $("#back").css("display", 'none')
        $("#update-form").css('display', 'none').trigger("reset")
        $("#form").trigger("reset")
        $("#form-multiple").trigger("reset")
        $("#form select").val('').change();
        $("#form-input select").val('').change();
        $("form").find("form-input").trigger("reset")
    }
</script>
