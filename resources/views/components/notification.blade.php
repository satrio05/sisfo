@push('js')
    <script>
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        function SwalConfirm(url, id) {
            // const SwalConfirm = Swal.fire({
            //     title: 'Are you sure?',
            //     text: "You won't be able to revert this!",
            //     icon: 'warning',
            //     showCancelButton: true,
            //     confirmButtonColor: '#3085d6',
            //     cancelButtonColor: '#d33',
            //     confirmButtonText: 'Yes, delete it!'
            // }).then((result) => {
            //     if (result.isConfirmed) {
            //         $.ajax({
            //             url: url,
            //             type: 'DELETE',
            //             data: {
            //                 id: id,
            //                 method: 'DELETE',
            //                 submit: true
            //             }
            //         }).done(function(data) {
            //             Swal.fire(
            //                 'Deleted!',
            //                 data.message,
            //                 'success'
            //             )
            //         }).fail(function(xhr, status, error, responseJSON) {
            //             let errors = xhr.responseJSON.errors
            //             let message = '';
            //             $.each(errors, function(value, index) {
            //                 message += `${errors[value]} \n`;
            //             })
            //             Toast.fire({
            //                 icon: 'error',
            //                 title: message
            //             })
            //         })
            //     }
            // })
        }
    </script>
@endpush
