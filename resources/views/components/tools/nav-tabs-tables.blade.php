<div class="card">
    <div class="card-header">
        <ul class="nav nav-tabs" id="{{ $id }}">
            @foreach ($config as $index => $item)
                <li class="nav-item">
                    <a class="nav-link {{ $item['status'] ?? '' }}" id="{{ $item['target'] }}" data-bs-toggle="tab"
                        type="button" href="#{{ $item['name'] }}" role="tab">{{ $item['target'] }}</a>
                </li>
            @endforeach
        </ul>
    </div>
    <div class="card-body">
        <div class="tab-content">
            @foreach ($config as $index => $value)
                <div class="tab-pane fade show {{ $value['status'] }}" id="{{ $value['name'] }}" role="tabpanel">
                    <div class="d-flex">
                        <div class="ml-auto mb-2">
                            @if (array_key_exists('action-nav-right', $value))
                                <x-form.button classes="{{ $value['id'] }}" :config="$value['action-nav-right']" />
                            @endif
                        </div>
                    </div>
                    @if ($value['id'] == 'types')
                        <div class="row">
                            <div class="col-md-6">
                                <table id='table-{{ $value['target'] }}'
                                    class="table table-hover table-bordered table-stripped display" width="100%">
                                </table>
                            </div>
                            <div class="col-md-6 ">
                                <div class="card card-success">
                                    <div class="card-header">
                                        <h3 class="card-title">{{ __('Form ') }} {{ __($value['target']) }}</h3>
                                    </div>
                                    @if (array_key_exists('types', $value))
                                        <div class="card-body">
                                            <form method="POST" id="form-input">
                                                @csrf
                                                <input type="hidden" name="type" value="{{ $value['types'] }}">
                                                @foreach ($value['add'] as $item => $values)
                                                    <x-form.input :config="$values"></x-form.input>
                                                @endforeach
                                                <x-adminlte-button type="submit" id="save-form" class="mr-auto"
                                                    theme="success" label="{{ __('Save') }}" data-type="form"
                                                    data-route="../{{ $value['id'] }}/{{ $value['id'] }}-add" />
                                                <x-adminlte-button type="submit" id="update-form" class="mr-auto"
                                                    theme="warning" label="{{ __('Update') }} {{ __($value['id']) }}"
                                                    data-type="form" data-route="{{ $value['id'] }}-update"
                                                    style="display: none" />
                                                <x-adminlte-button type="button" id="back" class="mr-auto"
                                                    theme="danger" label="{{ __('Cancel') }}" />
                                            </form>

                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    @else
                        <table id='table-{{ $value['target'] }}'
                            class="table table-hover table-bordered table-stripped display" width="100%">
                        </table>
                    @endif
                </div>
            @endforeach
        </div>
    </div>
