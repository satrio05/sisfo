@foreach ($config as $dropdown => $values)
    @if ($dropdown == 'delete')
        @php($action = 'destroy')
    @else
        @php($action = 'add')
    @endif
    @if (array_key_exists('dropdown', $values))
        <div class="btn-group">
            <button type="button" class="btn btn-{{ $values['theme'] }} dropdown-toggle dropdown-toggle-split"
                aria-haspopup="true" id="{{ $values['label'] }}" data-toggle="dropdown" href="#" aria-expanded="false">
                @isset($values['icon'])
                    <i class="{{ $values['icon'] }}"></i>
                @endisset
                @isset($values['label'])
                    {{ __($values['label']) }}
                @endisset
                <span class="sr-only">Toggle Dropdown</span>
            </button>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="{{ $values['label'] }}">
                @foreach ($values['dropdown'] as $dropdown_menu)
                    <button type="button" class="dropdown-item" data-route="{{ $classes }}-{{ $action }}"
                        id="{{ $dropdown_menu }}-{{ $action }}">{{ Str::title($dropdown_menu) }}</button>
                @endforeach
            </div>
        </div>
    @elseif(array_key_exists('action', $values))
        <button type="button" class="btn btn-{{ $values['theme'] }}"
            @if (array_key_exists('methodDelete', $values)) method-delete = "{{ $values['methodDelete'] }}" @endif
            data-route="{{ $values['route'] }}" id="{{ $values['action'] }}-{{ $action }}">
            @isset($values['icon'])
                <i class="{{ $values['icon'] }}"></i>
            @endisset
            @isset($values['label'])
                {{ Str::title(__($values['label'])) }}
            @endisset
        </button>
    @endif
@endforeach
