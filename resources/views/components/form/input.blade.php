@if ($type == 'file')
    <x-adminlte-input-file name="{{ $name }}" igroup-size="{{ $size }}"
        placeholder="{{ __('Choose a ') }}{{ Str::kebab($name) }} ">
        <x-slot name="prependSlot">
            <div class="input-group-text bg-lightblue">
                @isset($icon)
                    <i class="{{ $icon }}"></i>
                @endisset
            </div>
        </x-slot>
    </x-adminlte-input-file>
@endif
@if ($type == 'date')
    <x-adminlte-input-date :config="$params" name="{{ $name }}"
        placeholder="{{ __('Choose a date') }} {{ Str::kebab($name) }}"
        label="{{ __('Choose a date') }}  {{ Str::kebab($name) }}">
        <x-slot name="appendSlot">
            <div class="input-group-text bg-gradient-danger">
                <i class="{{ $icon }}"></i>
            </div>
        </x-slot>
    </x-adminlte-input-date>
@endif
@if ($type == 'text' || $type == 'number')
    <x-adminlte-input name="{{ $name }}" type="{{ $type }}" placeholder="{{ $placeholder }}"
        igroup-size="{{ $size }}" label-class="text-danger">
        <x-slot name="prependSlot">
            <div class="input-group-text bg-gradient-danger">
                <i class="{{ $icon }}"></i>
            </div>
        </x-slot>
    </x-adminlte-input>
@endif

@if ($type == 'textarea')
    <x-adminlte-textarea name="{{ $name }}" rows=2 igroup-size="md" placeholder="{{ $placeholder }}"
        disable-feedback>
        <x-slot name="prependSlot">
            <div class="input-group-text bg-gradient-danger">
                <i class="{{ $icon }}"></i>
            </div>
        </x-slot>
    </x-adminlte-textarea>
@endif
@if ($type == 'select-no-label' || $type == 'select')
    @if (!empty($route))
        @php($route = $route)
    @endif
    @if ($route != '' && $value == '')
        <x-adminlte-select2 name="{{ $name }}" placeholder="
        {{ $placeholder }}"
            data-name="{{ $name }}" igroup-size="md" data-route="{{ $route }}"
            label-class="text-lightblue" data-filter="{{ $global }}">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-gradient-red">
                    <i class="{{ $icon }}"></i>
                </div>
            </x-slot>
        </x-adminlte-select2>
    @else
        <x-adminlte-select2 name="{{ $name }}" data-name="{{ $name }}" igroup-size="md"
            label-class="text-lightblue" placeholder="{{ $placeholder }}">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-gradient-red">
                    <i class="{{ $icon }}"></i>
                </div>
            </x-slot>
            <x-adminlte-options :options="$value" empty-option="Select an option {{ $placeholder }}" />
        </x-adminlte-select2>
    @endif
@endif
@if ($type == 'select-multi')
    @if ($route != '')
        <x-adminlte-select2 id="{{ $name }}" name="{{ $name }}[]" igroup-size="{{ $size }}"
            multiple data-route="{{ $route }}" data-filter="{{ $global }}"
            placeholder="
            {{ $placeholder }}">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-gradient-red">
                    <i class="{{ $icon }}"></i>
                </div>
            </x-slot>
            <x-slot name="bottomSlot">
                <span class="text-sm text-danger">
                    Select an option {{ $name }}
                </span>
            </x-slot>
        </x-adminlte-select2>
    @else
        <x-adminlte-select2 id="{{ $name }}" name="{{ $name }}[]" placeholder="{{ $placeholder }}"
            igroup-size="md" multiple placeholder=" 
            {{ $placeholder }}">
            <x-slot name="prependSlot">
                <div class="input-group-text bg-gradient-red">
                    <i class="{{ $icon }}"></i>
                </div>
            </x-slot>
            <x-adminlte-options :options="$value"
                empty-option="Select an option
                {{ $placeholder }}" />
        </x-adminlte-select2>
    @endif
@endif

@if ($type == 'file-one')
    <x-adminlte-input-file name="{{ $name }}" igroup-size="md" placeholder="{{ $placeholder }}">
        <x-slot name="prependSlot">
            <div class="input-group-text bg-lightblue">
                <i class="fas fa-upload"></i>
            </div>
        </x-slot>
    </x-adminlte-input-file>
@endif
