    <div class="form-group">
        <label
            class="control-label @if (empty($size)) col-md-2 @else {{ $size }} @endif">{{ Str::ucfirst($name) }}</label>
        <label class="control-label badge-success mr-2 rounded" for="{{ $name }}" id="{{ $name }}"> :
        </label>
    </div>
