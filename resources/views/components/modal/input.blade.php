@if (is_null($ColumnsForm))
@else
    <x-adminlte-modal id="modalCustom" title="Account Policy" size="lg" theme="teal" icon="fas fa-fw fa-bell" v-centered
        static-backdrop>
        <form method="POST" id="form">
            @csrf
            <div class="row">
                @foreach ($ColumnsForm as $item => $values)
                    @php
                        $config = [
                            'text' => [
                                'name' => $values['name'],
                                'type' => 'text',
                                'icon' => $values['icon'],
                                'placeholder' => __('Entry your') . ' ' . Str::kebab($values['name']),
                            ],
                            'number' => [
                                'name' => $values['name'],
                                'type' => 'number',
                                'icon' => $values['icon'],
                                'placeholder' => __('since') . ' ' . __('Number Valid'),
                                'size' => 'md',
                            ],
                        ];
                    @endphp
                    @if ($values['type'] != 'textarea')
                        <div class="col-md-6">
                            @if ($values['type'] == 'text')
                                <x-form.input :config="$config['text']"></x-form.input>
                            @endif
                            @if ($values['type'] == 'number')
                                <x-form.input :config="$config['number']"></x-form.input>
                            @endif
                            @if ($values['type'] == 'select-no-label')
                                <x-adminlte-select2 name="{{ $values['name'] }}" igroup-size="md"
                                    label-class="text-lightblue">
                                    <x-slot name="prependSlot">
                                        <div class="input-group-text bg-gradient-danger">
                                            <i class="{{ $values['icon'] }}"></i>
                                        </div>
                                    </x-slot>
                                    @if (array_key_exists('value', $values))
                                        {{-- <option value="empty">S</option> --}}
                                        <x-adminlte-options :options="$values['value']"
                                            empty-option="Select an option
                                {{ $values['name'] }}" />
                                    @endif
                                </x-adminlte-select2>
                            @endif
                        </div>
                    @else
                        <div class="col-sm-12">
                            @if ($values['type'] == 'textarea')
                                <x-adminlte-textarea name="{{ $values['name'] }}" rows=2 label-class="text-warning"
                                    igroup-size="md" placeholder="Insert description. {{ $values['name'] }}">
                                    <x-slot name="prependSlot">
                                        <div class="input-group-text bg-dark">
                                            <i class="{{ $values['icon'] }} text-warning"></i>
                                        </div>
                                    </x-slot>
                                </x-adminlte-textarea>
                            @endif
                        </div>
                    @endif
                @endforeach
            </div>
        </form>
        <x-slot name="footerSlot">
            <x-adminlte-button theme="danger" label="Dismiss" data-dismiss="modal" />
        </x-slot>
    </x-adminlte-modal>
    <x-adminlte-modal id="modal-multiple-insert" theme="purple" icon="fa fa-fw fa-plus" size='lg'
        disable-animations>
        <form method="POST" id="form-multiple">
            <table class="table table-sm" id="multiple-table">
                <thead>
                    <tr>
                        @foreach ($ColumnsForm as $item => $values)
                            <th>{{ Str::kebab($values['name']) }}</th>
                        @endforeach
                        <th>{{ __('Action') }}</th>
                    </tr>
                </thead>
                <tbody>
                    @csrf
                    <input type="hidden" name="types" value="multiple">
                    <tr id="column-insert">
                        @foreach ($ColumnsForm as $item => $values)
                            <td>
                                @if ($values['type'] == 'text')
                                    <x-adminlte-input name="{{ $values['name'] }}[]" data-name="{{ $values['name'] }}"
                                        type="text" placeholder="{{ __('Entry your') }} {{ $values['name'] }}" />
                                @endif
                                @if ($values['type'] == 'number')
                                    <x-adminlte-input name="{{ $values['name'] }}[]" type="number"
                                        data-name="{{ $values['name'] }}"
                                        placeholder="{{ __('Entry your') }} {{ $values['name'] }}" />
                                @endif
                                @if ($values['type'] == 'select-no-label')
                                    <x-adminlte-select2 name="{{ $values['name'] }}[]"
                                        data-name="{{ $values['name'] }}" igroup-size="md"
                                        label-class="text-lightblue">
                                        @if (array_key_exists('value', $values))
                                            {{-- <option value="empty">S</option> --}}
                                            <x-adminlte-options :options="$values['value']"
                                                empty-option="Select an option
                                {{ $values['name'] }}" />
                                        @endif
                                    </x-adminlte-select2>
                                @endif
                                @if ($values['type'] == 'select')
                                    <x-adminlte-select2 name="{{ $values['name'] }}[]"
                                        data-name="{{ $values['name'] }}" igroup-size="md"
                                        label-class="text-lightblue">
                                        @if (array_key_exists('value', $values))
                                            {{-- <option value="empty">S</option> --}}
                                            <x-adminlte-options :options="$values['value']"
                                                empty-option="Select an option
                                {{ $values['name'] }}" />
                                        @endif
                                    </x-adminlte-select2>
                                @endif
                                @if ($values['type'] == 'textarea')
                                    <x-adminlte-textarea name="{{ $values['name'] }}[]" rows=2
                                        label-class="text-warning" data-name="{{ $values['name'] }}"
                                        placeholder="Insert description. {{ $values['name'] }}">
                                    </x-adminlte-textarea>
                                @endif
                                @if ($values['type'] == 'file-one')
                                    {{-- ini kak html nyo --}}
                                    <x-adminlte-input-file name="{{ $values['name'] }}[]"
                                        data-name="{{ $values['name'] }}" igroup-size="md"
                                        placeholder="{{ $values['placeholder'] }}">
                                        <x-slot name="prependSlot">
                                            <div class="input-group-text bg-lightblue">
                                                <i class="fas fa-upload"></i>
                                            </div>
                                        </x-slot>
                                    </x-adminlte-input-file>
                                @endif

                            </td>
                        @endforeach
                        <td>
                            <x-adminlte-button type="button" id="add-new-row" data-row="0" class="mr-auto"
                                theme="success" icon="fa fa-fw fa-plus" />
                        </td>
                    </tr>
                </tbody>
                </div>
            </table>
            <div class="fields">
        </form>
    </x-adminlte-modal>
@endif

@push('js')
    <script>
        let row = 0
        $(document).on('click', '#add-new-row', function(e) {
            row++
            let clone = $('table#multiple-table tr#column-insert').clone().attr('id',
                `column-insert-${row}`).insertBefore('tr#column-insert')
            $(`#column-insert-${row}`).find('#add-new-row:last').attr('id', 'remove-row').removeClass(
                'btn-success').addClass('btn-danger').html(
                '<i class="fa fa-fw fa-minus"></i>');
            $(`#column-insert-${row}`).find(':input, textarea,input[type="file"]').map(function(index, value) {
                let name = $(value).data('name')
                let id = $(value).attr('id')
                let changeName = `${name}[]`
                $(`#column-insert-${row}`).find(
                        `input[name="${id}"],textarea[name="${id}"]`)
                    .attr({
                        'name': changeName,
                        'id': changeName
                    }).val('')
                $(`#column-insert-${row} button[id="remove-row"]`).attr({
                    'data-row': row
                })
            })
        })

        $(document).on('click', `#remove-row`, function(e) {
            e.preventDefault()
            $('#column-insert-' + $(this).data('row')).remove()
        })
    </script>
@endpush
