@php
$button = config('adminlte.buttons-form');
@endphp
@foreach ($config as $keys => $values)
    <x-adminlte-modal id="modal-{{ $keys }}" title="{{ __($values['title']) }}  " theme="purple"
        icon="fa fa-file" size='lg' disable-animations>
        @if (array_key_exists('required', $values))
            @foreach ($values['required'] as $required => $validateRequired)
                @if (gettype($validateRequired) == 'array')
                    @foreach ($validateRequired as $download)
                        <x-adminlte-button type="submit" id="download-format" class="mr-auto mb-2" theme="success"
                            label="Download format" icon="{{ $download['icon'] }}"
                            data-route="{{ $download['route'] }}" />
                    @endforeach
                @else
                    <label for="" class="text-danger bold mb-2">{{ $validateRequired }}</label><br>
                @endif
            @endforeach
        @endif
        <form enctype='multipart/form-data' id="form-{{ $keys }}" method="POST">
            @csrf
            @if ($keys == 'import')
                @foreach ($values['columns'] as $input)
                    @php
                        $config = [
                            'file' => [
                                'name' => $input['name'],
                                'type' => 'file',
                                'icon' => $input['icon'],
                                'size' => 'md',
                            ],
                        ];
                    @endphp
                    @if ($input['type'] == 'file')
                        <x-form.input :config="$config['file']"></x-form.input>
                    @endif
                @endforeach
            @else
                <div class="row">
                    @foreach ($values['columns'] as $input)
                        @php
                            $config = [
                                'date' => [
                                    'name' => $input['name'],
                                    'type' => 'date',
                                    'icon' => 'fas fa-calendar-alt',
                                    'size' => 'md',
                                ],
                            ];
                        @endphp
                        @if ($input['type'] == 'date')
                            <div class="col-md-6">
                                <x-form.input :config="$config['date']" :params="$input['config']"></x-form.input>
                            </div>
                        @endif
                        @if ($input['type'] == 'select')
                        @endif
                    @endforeach
                </div>
            @endif
        </form>
        <x-slot name="footerSlot">
            @if ($keys == 'import')
                @foreach ($button['import'] as $index => $buttonImport)
                    <x-adminlte-button type="submit" id="save-{{ $keys }}" class="mr-auto" theme="success"
                        label="{{ $buttonImport['name'] }}" data-name="{{ $buttonImport['id'] }}"
                        icon="{{ $buttonImport['icon'] }}" />
                @endforeach
            @endif
            @if ($keys == 'export')
                @foreach ($button['export'] as $index => $buttonExport)
                    <x-adminlte-button type="submit" id="downloads-{{ $keys }}" class="float-end"
                        theme="primary" label="{{ $buttonExport['name'] }}" data-name="{{ $buttonExport['id'] }}"
                        icon="{{ $buttonExport['icon'] }}" />
                @endforeach
            @endif
            <x-adminlte-button theme="danger" label="Dismiss" data-dismiss="modal" />
        </x-slot>
    </x-adminlte-modal>
@endforeach
