<x-adminlte-modal id="modalCustom" title="Account Policy" size="md" theme="teal" icon="fas fa-bell" v-centered
    static-backdrop>
    <form method="POST" id="form">
        @csrf
        <div class="row">
            @foreach ($ColumnsForm as $item => $values)
                <div class="col-md-6">
                    @if ($values['type'] == 'text')
                        <x-adminlte-input name="{{ $values['name'] }}"
                            placeholder="{{ __('Entry your') }} {{ Str::kebab($values['name']) }}">
                            <x-slot name="prependSlot">
                                <div class="input-group-text bg-gradient-danger">
                                    <i class="{{ $values['icon'] }}"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input>
                    @endif
                    @if ($values['type'] == 'number')
                        <x-adminlte-input name="{{ $values['name'] }}"
                            placeholder="{{ __('since') }} {{ __('Number Valid') }} " type="number">
                            <x-slot name="prependSlot">
                                <div class="input-group-text bg-gradient-danger">
                                    <i class="{{ $values['icon'] }}"></i>
                                </div>
                            </x-slot>
                        </x-adminlte-input>
                    @endif
                    @if ($values['type'] == 'select')
                        <x-adminlte-select2 name="{{ $values['name'] }}" igroup-size="md"
                            label-class="text-lightblue">
                            <x-slot name="prependSlot">
                                <div class="input-group-text bg-gradient-danger">
                                    <i class="{{ $values['icon'] }}"></i>
                                </div>
                            </x-slot>
                            @if (array_key_exists('value', $values))
                                {{-- <option value="empty">S</option> --}}
                                <x-adminlte-options :options="$values['value']"
                                    empty-option="Select an option
                                {{ $values['name'] }}" />
                            @endif
                        </x-adminlte-select2>
                    @endif
                </div>
            @endforeach
        </div>
    </form>
    <x-slot name="footerSlot">
        {{-- <x-adminlte-button class="mr-auto" theme="success" label="Accept" /> --}}
        <x-adminlte-button theme="danger" label="Dismiss" data-dismiss="modal" />
    </x-slot>
</x-adminlte-modal>