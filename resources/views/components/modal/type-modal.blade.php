@if ($animation)
    <x-adminlte-modal id="newModal" title="Account Policy" size="{{ $size ?? 'md' }}" theme="{{ $theme ?? 'teal' }}"
        icon="{{ $icon }}" v-centered static-backdrop>
        {{ $slot }}
    </x-adminlte-modal>
@else
    <x-adminlte-modal id="modalNoAnimation" title="aaa" theme="{{ $theme }}" icon="{{ $icon }}"
        size='{{ $size }}' disable-animations>
        {{ $slot }}
    </x-adminlte-modal>
@endif
