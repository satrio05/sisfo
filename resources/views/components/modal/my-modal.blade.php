@if ($type == 'Form-input')
    <x-adminlte-modal id="newModal" title="Account Policy" size="lg" theme="teal" icon="fa fa-fw fa-plus" v-centered
        static-backdrop>
        {{ $slot }}
    </x-adminlte-modal>
@else
    <x-adminlte-modal id="newModal" title="aaa" theme="purple" size='lg' disable-animations>
        {{ $slot }}
    </x-adminlte-modal>
@endif
